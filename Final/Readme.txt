Tổng kết toàn bộ phân công việc
Nguyễn Đình Hùng: Thiết kế use case Thuê xe mới + subsystem. Hoàn thiện tài liệu báo cáo nhóm final
Phạm Trung Hiếu: Thiết kế use case Thêm loại xe mới + Thiết kế database. Hoàn thiện báo cáo nhóm final
Phạm Hữu Anh Quốc: Thiết kế use case Trả xe.
Nguyễn Duy Long: Thiết kế use case thêm bãi xe mới. Hoàn thiện báo cáo nhóm final
Nguyễn Bá Hoàng: Thiết kế use case tìm kiếm bãi xe

Tổng % đóng góp của các thành viên trong nhóm vào kết quả chung
Nguyễn Đình Hùng: 30%
Phạm Trung Hiếu: 25%
Phạm Hữu Anh Quốc: 15%
Nguyễn Duy Long: 15%
Nguyễn Bá Hoàng: 15%
