package model.bike;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public enum BikeStatus {

    AVAILABLE("AVAILABLE"),
    RENTED("RENTED"),
    OUT_OF_BATTERY("OUT_OF_BATTERY"),
    RENTING_IN_PROGRESS("RENTING_IN_PROGRESS");

    private String name;

    public static BikeStatus findByName(String name) {
        for (BikeStatus status : BikeStatus.values()) {
            if (status.getName().equalsIgnoreCase(name)) {
                return status;
            }
        }
        return null;
    }
}
