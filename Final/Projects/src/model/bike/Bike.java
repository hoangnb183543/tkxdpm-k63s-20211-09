package model.bike;

import lombok.*;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class Bike {

    private long bikeNo;
    private String name;
    private String type;
    private double weight;
    private String licensePlate;
    private LocalDate manufacturingDate;
    private String producer;
    private int cost;
    private BikeStatus status;
    private String bikeCode;

}
