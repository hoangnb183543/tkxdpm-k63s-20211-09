package model.payment;

import lombok.*;

@Getter
@Setter
@ToString
public class CreditCard extends PaymentCard {

    private String bankIssue;

    @Builder
    public CreditCard(String owner, String cardCode, String dateExpired, String cvvCode, String bankIssue) {
        super(owner, cardCode, dateExpired, cvvCode);
        this.bankIssue = bankIssue;
    }

    public static class CreditCardBuilder extends PaymentCardBuilder {
        CreditCardBuilder() {
            super();
        }
    }

}
