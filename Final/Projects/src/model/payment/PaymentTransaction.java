package model.payment;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class PaymentTransaction {

    private String transactionNo;
    private String content;
    private double amount;
    private LocalDateTime createdAt;
    private PaymentCode paymentCode;

}
