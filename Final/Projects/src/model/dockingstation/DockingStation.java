package model.dockingstation;


import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString

public class DockingStation {
	
	private long dockingStationNo;
	private String name;
	private String address;
	private long capacity;
}
