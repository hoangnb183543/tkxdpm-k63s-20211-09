package model.rentalhistory;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class BikeRentalHistory {

    private long historyNo;
    private long bikeNo;
    private long userNo;
    private LocalDateTime createdOn;
    private UserAction userAction;

}
