package paymentsubsystem;

import model.payment.PaymentCard;
import model.payment.PaymentTransaction;

/**
 * @author hungnd
 */
public interface PaymentSubSystem {

    PaymentTransaction payOrder(PaymentCard card, double amount, String content);

}
