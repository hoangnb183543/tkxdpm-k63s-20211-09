package paymentsubsystem.impl;

public interface APICaller {

    String patch(String url, String token, String requestBody);

}
