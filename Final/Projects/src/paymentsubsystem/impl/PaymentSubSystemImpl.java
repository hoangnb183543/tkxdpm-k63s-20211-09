package paymentsubsystem.impl;

import com.google.gson.Gson;
import model.payment.PaymentCard;
import model.payment.PaymentCode;
import model.payment.PaymentTransaction;
import paymentsubsystem.PaymentSubSystem;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class PaymentSubSystemImpl implements PaymentSubSystem {

    private static final String YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
    private static final String COMMAND = "pay";
    private static final String VERSION = "1.0.0";
    private static final String OWNER = "Group 9";
    private static final String CARD_CODE = "kscq2_group9_2021";
    private static final String CVV_CODE = "619";
    private static final String DATE_EXPIRED = "1125";
    private static final String URL = "https://ecopark-system-api.herokuapp.com/api/card/processTransaction";
    private static final String API_TOKEN = "";

    @Override
    public PaymentTransaction payOrder(PaymentCard card, double amount, String content) {
        PayOrderRequestDTO requestDTO = PayOrderRequestDTO.builder()
                .dateTimeFormatter(YYYY_MM_DD_HH_MM_SS)
                .card(card)
                .amount(amount)
                .command(COMMAND)
                .version(VERSION)
                .content(content)
                .build();
        APICaller apiCaller = new APICallerImpl();
        String response = apiCaller.patch(URL, API_TOKEN, new Gson().toJson(requestDTO));
        PayOrderResponseDTO responseDTO = convertToPaymentTransaction(response);
        PaymentCode errorCode = PaymentCode.findByCode(responseDTO.getErrorCode());
        if (errorCode == PaymentCode.SUCCESS) {
            return PaymentTransaction.builder()
                    .transactionNo(responseDTO.getTransaction().getTransactionId())
                    .amount(responseDTO.getTransaction().getAmount())
                    .content(responseDTO.getTransaction().getTransactionContent())
                    .paymentCode(PaymentCode.findByCode(responseDTO.getErrorCode()))
                    .createdAt(LocalDateTime.parse(
                            responseDTO.getTransaction().getCreatedAt(), DateTimeFormatter.ofPattern(YYYY_MM_DD_HH_MM_SS)))
                    .build();
        } else {
            return PaymentTransaction.builder()
                    .paymentCode(errorCode)
                    .build();
        }
    }


    private PayOrderResponseDTO convertToPaymentTransaction(String response) {
        return new Gson().fromJson(response, PayOrderResponseDTO.class);
    }

}
