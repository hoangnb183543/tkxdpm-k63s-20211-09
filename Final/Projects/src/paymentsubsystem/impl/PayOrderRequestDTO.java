package paymentsubsystem.impl;

import lombok.Builder;
import lombok.Data;
import model.payment.PaymentCard;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class PayOrderRequestDTO {

    private String version;
    private Transaction transaction;

    @Builder
    private static class Transaction {
        private String cardCode;
        private String owner;
        private String cvvCode;
        private String dateExpired;
        private String command;
        private String transactionContent;
        private double amount;
        private String createdAt;
    }

    @Builder
    public PayOrderRequestDTO(String version, PaymentCard card, double amount,
                              String content, String dateTimeFormatter, String command) {
        this.version = version;
        this.transaction = Transaction.builder()
                .cardCode(card.getCardCode())
                .owner(card.getOwner())
                .cvvCode(card.getCvvCode())
                .dateExpired(card.getDateExpired())
                .command(command)
                .transactionContent(content)
                .amount(amount)
                .createdAt(LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimeFormatter)))
                .build();
    }

}
