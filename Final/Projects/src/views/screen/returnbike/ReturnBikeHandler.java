<<<<<<< HEAD
package views.screen.returnbike;

import controller.BikeInfoController;
import controller.ReturnBikeController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import model.rentalhistory.BikeRentalHistory;

import utils.Configs;
import utils.CurrencyUtils;
import utils.rentalprice.CalculateRentalFee;
import utils.rentalprice.CalculateRentalFeeByProgressiveFormula;
import views.screen.BaseScreenHandler;
import views.screen.refundscreen.RefundScreenHandler;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

@Log
public class ReturnBikeHandler extends BaseScreenHandler {

    @FXML
    private Label bikeName;
    @FXML
    private Label bikeType;
    @FXML
    private Label rentTime;
    @FXML
    private Label deposit;
    @FXML
    private Label price;
    @FXML
    private ChoiceBox<String> selectStationChoiceBox;
    @FXML
    private Button backBtn;
    @FXML
    private Button confirmBtn;
    @FXML
    private Button getBtn;
    @FXML
    private ImageView ecobikeImg;

    private double cost;
    private double depositPrice;
  
    ObservableList<Long> stationIds;
    private Long stationId;
    ObservableList<String> stationNames;
    
    private ReturnBikeController controller;
    private BikeInfoController bikeInfoController;
    private long bikeNo;
  
    public ReturnBikeHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath, true); 
		this.controller = new ReturnBikeController();
		this.bikeInfoController = new BikeInfoController();
		
		
		BikeRentalHistory currentHistory = controller.getLastHistory();
		this.bikeNo = currentHistory.getBikeNo();
		Map<String, Object> bikeInfo = bikeInfoController.getBikeInfo(currentHistory.getBikeNo());

		long bikeType = 1;
		
		CalculateRentalFee calcuteRentalFee = new CalculateRentalFeeByProgressiveFormula(bikeType);

		long seconds = ChronoUnit.SECONDS.between(currentHistory.getCreatedOn(), LocalDateTime.now());
		
		this.cost = calcuteRentalFee.getRentalPrice(seconds);		
		this.depositPrice = this.bikeInfoController.getDepositAmtByBikeNo(currentHistory.getBikeNo());
		
		updateData(bikeInfo, currentHistory);
		
		stationIds = FXCollections.observableArrayList();
		stationNames = FXCollections.observableArrayList();
		
		List<DockingStation> stations = controller.findListDockingStation();
		
		for (DockingStation station : stations) {
		    stationIds.add(station.getDockingStationNo());
		    stationNames.add(station.getName());
		}
		
		selectStationChoiceBox.setItems(stationNames);
		
		selectStationChoiceBox.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal)->{
		    int index = stationNames.indexOf(newVal);
		    this.stationId = stationIds.get(index);
		    log.info(this.stationId.toString());
		    this.confirmBtn.setDisable(false);
		});

        backBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                getPreviousScreen().show();
            }
        });
	}
    
    @FXML
    private void confirmBtnClicked(MouseEvent event) throws IOException {
    	 log.info("Confirm button clicked");
    	 RefundScreenHandler handler = new RefundScreenHandler(this.stage, Configs.REFUND_PATH, this.depositPrice - this.cost, this.bikeNo);
       
    	 handler.setHomeScreenHandler(homeScreenHandler);
    	 handler.setScreenTitle("Refund screen");
    	 handler.setPreviousScreen(prev);
    	 handler.show();
    }
    
    private void updateData(Map<String, Object> bikeInfo, BikeRentalHistory currentHistory) {
        bikeName.setText((String) bikeInfo.get(BikeInfoController.BIKE_NAME));
        bikeType.setText((String) bikeInfo.get(BikeInfoController.BIKE_TYPE));
        price.setText(CurrencyUtils.convertToString((double) this.cost));
        deposit.setText(CurrencyUtils.convertToString((double) this.depositPrice));
        DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");
        rentTime.setText(currentHistory.getCreatedOn().format(FORMATTER));
    }
    
}
=======
package views.screen.returnbike;

import controller.BikeInfoController;
import controller.ReturnBikeController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import model.rentalhistory.BikeRentalHistory;

import utils.Configs;
import utils.CurrencyUtils;
import utils.rentalprice.CalculateRentalFee;
import utils.rentalprice.CalculateRentalFeeByProgressiveFormula;
import views.screen.BaseScreenHandler;
import views.screen.refundscreen.RefundScreenHandler;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Map;

@Log
public class ReturnBikeHandler extends BaseScreenHandler {

    @FXML
    private Label bikeName;
    @FXML
    private Label bikeType;
    @FXML
    private Label rentTime;
    @FXML
    private Label deposit;
    @FXML
    private Label price;
    @FXML
    private ChoiceBox<String> selectStationChoiceBox;
    @FXML
    private Button backBtn;
    @FXML
    private Button confirmBtn;
    @FXML
    private Button getBtn;
    @FXML
    private ImageView ecobikeImg;

    private double cost;
    private double depositPrice;
  
    ObservableList<Long> stationIds;
    private Long stationId;
    ObservableList<String> stationNames;
    
    private ReturnBikeController controller;
    private BikeInfoController bikeInfoController;
    private long bikeNo;
  
    public ReturnBikeHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath, true); 
		this.controller = new ReturnBikeController();
		this.bikeInfoController = new BikeInfoController();
		
		
		BikeRentalHistory currentHistory = controller.getLastHistory();
		this.bikeNo = currentHistory.getBikeNo();
		Map<String, Object> bikeInfo = bikeInfoController.getBikeInfo(currentHistory.getBikeNo());

		long bikeType = 1;
		
		CalculateRentalFee calcuteRentalFee = new CalculateRentalFeeByProgressiveFormula(bikeType);

		long seconds = ChronoUnit.SECONDS.between(currentHistory.getCreatedOn(), LocalDateTime.now());
		
		this.cost = calcuteRentalFee.getRentalPrice(seconds);		
		this.depositPrice = this.bikeInfoController.getDepositAmtByBikeNo(currentHistory.getBikeNo());
		
		updateData(bikeInfo, currentHistory);
		
		stationIds = FXCollections.observableArrayList();
		stationNames = FXCollections.observableArrayList();
		
		List<DockingStation> stations = controller.findListDockingStation();
		
		for (DockingStation station : stations) {
		    stationIds.add(station.getDockingStationNo());
		    stationNames.add(station.getName());
		}
		
		selectStationChoiceBox.setItems(stationNames);
		
		selectStationChoiceBox.getSelectionModel().selectedItemProperty().addListener((obs, oldVal, newVal)->{
		    int index = stationNames.indexOf(newVal);
		    this.stationId = stationIds.get(index);
		    log.info(this.stationId.toString());
		    this.confirmBtn.setDisable(false);
		});

        backBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                getPreviousScreen().show();
            }
        });
	}
    
    @FXML
    private void confirmBtnClicked(MouseEvent event) throws IOException {
    	 log.info("Confirm button clicked");
    	 RefundScreenHandler handler = new RefundScreenHandler(this.stage, Configs.REFUND_PATH, this.depositPrice - this.cost, this.bikeNo);
       
    	 handler.setHomeScreenHandler(homeScreenHandler);
    	 handler.setScreenTitle("Refund screen");
    	 handler.setPreviousScreen(prev);
    	 handler.show();
    }
    
    private void updateData(Map<String, Object> bikeInfo, BikeRentalHistory currentHistory) {
        bikeName.setText((String) bikeInfo.get(BikeInfoController.BIKE_NAME));
        bikeType.setText((String) bikeInfo.get(BikeInfoController.BIKE_TYPE));
        price.setText(CurrencyUtils.convertToString((double) this.cost));
        deposit.setText(CurrencyUtils.convertToString((double) this.depositPrice));
        DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");
        rentTime.setText(currentHistory.getCreatedOn().format(FORMATTER));
    }
    
}
>>>>>>> e3df24bc663682e1850609286bf005a948322edf
