package views.screen.returnbike;

import java.io.IOException;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

public class NotFoundBikeHandler extends BaseScreenHandler {
	
	@FXML
    private Button homeBtn;
	public NotFoundBikeHandler(Stage stage, String screenPath) throws IOException {
		super(stage, screenPath, true);
		homeBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                homeScreenHandler.show();
            }
        });
	}

}
