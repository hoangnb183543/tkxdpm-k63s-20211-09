package views.screen.searchdockingstation;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import controller.DockingStationSearchController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.bikesearch.BikeInfoScreenHandler;
import views.screen.bikesearch.BikeSearchScreenHandler;

@Log
public abstract class DockingStationSearchFormHandler extends BaseScreenHandler {
	
	DockingStationSearchController controller;
	
	@FXML
	private Text typeParkingSearch;
    	@FXML
    	private TableView<DockingStation> tableView;
	@FXML
	private TableColumn<DockingStation, String> codeTableColumn;
	@FXML
	private TableColumn<DockingStation, String> nameTableColumn;
	@FXML
	private TableColumn<DockingStation, String> addressTableColumn;
	@FXML
	private TextField keywordTxtField;
	
	ObservableList<DockingStation> dockingStationObservableList;
	
	public DockingStationSearchFormHandler(Stage stage, String screenpath, String type) throws IOException {
		super(stage, screenpath, true);
		this.controller = new DockingStationSearchController();
		typeParkingSearch.setText(type);
		setTableViewData();
		setTableViewClicked(stage, this);
	}
	
	protected abstract boolean isContain(DockingStation d, String search);
	
	private void setTableViewData() {
		dockingStationObservableList = FXCollections.observableArrayList();
		dockingStationObservableList.addAll(controller.findAllDockingStations());
		codeTableColumn.setCellValueFactory(new PropertyValueFactory<>("dockingStationNo"));
		nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
		addressTableColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
		tableView.setItems(dockingStationObservableList);
    	FilteredList<DockingStation> filteredList = new FilteredList<>(dockingStationObservableList, d -> true);
		keywordTxtField.textProperty().addListener(((observableValue, oldValue, newValue) -> {
			filteredList.setPredicate(dockingStation -> {
				if(newValue.isEmpty() || newValue.isBlank()) {
					return true;
				}
				
				String search = newValue.toLowerCase();
				return isContain(dockingStation, search);
			});
		}));
		
		SortedList<DockingStation> sortedList = new SortedList<>(filteredList);
		sortedList.comparatorProperty().bind(tableView.comparatorProperty());
		tableView.setItems(sortedList);
	}

	private void setTableViewClicked(Stage stage, BaseScreenHandler prev) {
		tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				if (event.getClickCount() == 2) {
					try {
						Map<String, Object> messages = new HashMap<>();
						long stationNo = tableView.getSelectionModel().getSelectedItem().getDockingStationNo();
						String stationName = tableView.getSelectionModel().getSelectedItem().getName();
						String nextScreenTitle = String.format("%s in station %s", BikeSearchScreenHandler.SCREEN_TITLE,
								stationName);
						messages.put(BikeSearchScreenHandler.STATION_NO, stationNo);

						BikeSearchScreenHandler bikeSearchScreenHandler = new BikeSearchScreenHandler(stage,
								Configs.SEARCH_BIKE_SCREEN_PATH,
								messages);
						bikeSearchScreenHandler.setHomeScreenHandler(homeScreenHandler);
						bikeSearchScreenHandler.setPreviousScreen(prev);
						bikeSearchScreenHandler.setScreenTitle(nextScreenTitle);
						bikeSearchScreenHandler.show();

					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		});
	}
    
}
