package views.screen.parking;

import java.io.IOException;

import java.io.IOException;
import java.util.Hashtable;

import controller.BikeSearchController;
import controller.ParkingController;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import utils.Configs;
import utils.DateUtils;
import views.screen.BaseScreenHandler;
import views.screen.bikesearch.BikeInfoScreenHandler;
import views.screen.bikesearch.BikeSearchScreenHandler;

@Log
public class ParkingScreenHandler extends BaseScreenHandler {
	@FXML
    private ImageView ecobikeImg;
    @FXML
    private TextField keywordTxtField;
    @FXML
    private TableView<DockingStation> tableView;
    @FXML
    private TableColumn<DockingStation, String> codeTableColumn;
    @FXML
    private TableColumn<DockingStation, String> nameTableColumn;
    @FXML
    private TableColumn<DockingStation, String> descriptionTableColumn;
    @FXML
    private TableColumn<DockingStation, Double> depositAmtTableColumn;

    ObservableList<DockingStation> dockingStationObservableList;
	ParkingController controller;
	
	public ParkingScreenHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath, true);
        this.controller = new ParkingController();
        setTableViewData();
//        setTableViewClicked(stage, this);
    }
	
	@FXML
    private void addParkingBtnClicked(MouseEvent event) throws IOException {		
		log.info("Add bike type button clicked");
		AddParkingHandler addParkingHandler;
		addParkingHandler = new AddParkingHandler(stage, Configs.ADD_PARKING_SCREEN_PATH);
		addParkingHandler.setHomeScreenHandler(homeScreenHandler);
		addParkingHandler.setScreenTitle("Add Docking Station");
		addParkingHandler.setPreviousScreen(this);
		addParkingHandler.show();	   
    }
	
	private void setTableViewData() {
		dockingStationObservableList = FXCollections.observableArrayList();
		dockingStationObservableList.addAll(controller.getAllDockingStations());
        codeTableColumn.setCellValueFactory(new PropertyValueFactory<>("dockingStationNo"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        descriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("address"));
        depositAmtTableColumn.setCellValueFactory(new PropertyValueFactory<>("capacity"));
       
        tableView.setItems(dockingStationObservableList);

        
    }

    private boolean isContain(String field, String search) {
        return field.toLowerCase().contains(search);
    }
}
