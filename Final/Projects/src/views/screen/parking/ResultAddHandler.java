package views.screen.parking;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import utils.Configs;
import views.screen.BaseScreenHandler;
import lombok.extern.java.Log;

@Log
public class ResultAddHandler extends BaseScreenHandler {
	@FXML
    private ImageView ecobikeImg;
	
	public ResultAddHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath, true);
    }
	
	@FXML
    private void backBtnClicked(MouseEvent event) throws IOException {		
		log.info("BackToManager button clicked");
		ParkingScreenHandler ParkingHandler;
		ParkingHandler = new ParkingScreenHandler(stage, Configs.PARKING_MANAGER_PATH);
		ParkingHandler.setHomeScreenHandler(homeScreenHandler);
		ParkingHandler.setScreenTitle("Docking Station Management");
		ParkingHandler.setPreviousScreen(this);
		ParkingHandler.show();	   
    }
}
