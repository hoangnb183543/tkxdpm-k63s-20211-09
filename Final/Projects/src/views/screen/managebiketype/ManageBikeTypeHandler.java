package views.screen.managebiketype;

import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Optional;

import controller.BikeSearchController;
import controller.BikeTypeController;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.bike.Bike;
import model.biketype.BikeType;
import utils.Configs;
import utils.DateUtils;
import views.screen.BaseScreenHandler;
import views.screen.bikesearch.BikeInfoScreenHandler;
import views.screen.bikesearch.BikeSearchScreenHandler;

@Log
public class ManageBikeTypeHandler extends BaseScreenHandler {
	@FXML
    private ImageView ecobikeImg;
    @FXML
    private TextField keywordTxtField;
    @FXML
    private TableView<BikeType> tableView;
    @FXML
    private TableColumn<BikeType, String> codeTableColumn;
    @FXML
    private TableColumn<BikeType, String> nameTableColumn;
    @FXML
    private TableColumn<BikeType, String> descriptionTableColumn;
    @FXML
    private TableColumn<BikeType, Double> depositAmtTableColumn;
    @FXML
    private TableColumn<BikeType, BikeType> actionColumn;
    @FXML
    private Button addBikeTypeBtn;
    @FXML
    private Button backBtn;
    
    ObservableList<BikeType> bikeTypeObservableList;

    BikeTypeController controller;

    private final String SCREEN_TITLE = "Bike type management";

    public ManageBikeTypeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath, true);
        this.controller = new BikeTypeController();
        
        backBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Map<String, Object> messages = new HashMap<>();
                getPreviousScreen().forward(messages);
                getPreviousScreen().show();
            }
        });
        
        setTableViewData();
        setTableViewClicked(stage, this);
    }

    /**
     * Thiết lập sự kiện click dòng trên table
     * @param stage
     * @param prev
     */
    private void setTableViewClicked(Stage stage, BaseScreenHandler prev) {
        tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    try {
                        BikeType selectedBikeType = tableView.getSelectionModel().getSelectedItem();
                        showEditBikeTypeForm(selectedBikeType);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
    
    @FXML
    private void addBikeTypeBtnClicked(MouseEvent event) throws IOException {		
		log.info("Add bike type button clicked");
		showAddBikeTypeForm();   
    }

    /**
     * Phương thức thiết lập dữ liệu cho table
     */
    private void setTableViewData() {
        bikeTypeObservableList = FXCollections.observableArrayList();
        bikeTypeObservableList.addAll(controller.getAllBikeTypes());
        codeTableColumn.setCellValueFactory(new PropertyValueFactory<>("bikeTypeCode"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("bikeTypeName"));
        descriptionTableColumn.setCellValueFactory(new PropertyValueFactory<>("bikeTypeDescription"));
        depositAmtTableColumn.setCellValueFactory(new PropertyValueFactory<>("depositAmount"));
        
        actionColumn.setCellValueFactory(param -> new ReadOnlyObjectWrapper<>(param.getValue()));
        actionColumn.setCellFactory(param -> new TableCell<BikeType, BikeType>() {
        	private final Button editBtn = new Button("Edit");
			private final Button deleteBtn = new Button("Delete");

            @Override
            protected void updateItem(BikeType bikeType, boolean empty) {
                super.updateItem(bikeType, empty);

                if (bikeType == null) {
                    setGraphic(null);
                    return;
                }

                HBox container = new HBox(editBtn, deleteBtn);
				container.setSpacing(10);
				container.setPadding(new Insets(2, 10, 2, 10));
				HBox.setHgrow(editBtn, Priority.ALWAYS);
	     		HBox.setHgrow(deleteBtn, Priority.ALWAYS);
	     		editBtn.setMaxWidth(Double.MAX_VALUE);
	     		deleteBtn.setMaxWidth(Double.MAX_VALUE);
	     		
                setGraphic(container);
                
                editBtn.setOnAction(
                    event -> {
						try {
							showEditBikeTypeForm(bikeType);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
                );
                deleteBtn.setOnAction(
                    event -> deleteAction(bikeType)
                );
            }
        });
        
        tableView.setItems(bikeTypeObservableList);

        FilteredList<BikeType> filteredList = new FilteredList<>(bikeTypeObservableList, b -> true);
        keywordTxtField.textProperty().addListener(((observableValue, oldValue, newValue) -> {
            filteredList.setPredicate(bikeType -> {
                if (newValue.isEmpty() || newValue.isBlank()) {
                    return true;
                }

                String search = newValue.toLowerCase();
                return isContain(bikeType.getBikeTypeCode(), search) || isContain(bikeType.getBikeTypeName(), search);
            });
        }));

        SortedList<BikeType> sortedList = new SortedList<>(filteredList);
        sortedList.comparatorProperty().bind(tableView.comparatorProperty());
        tableView.setItems(sortedList);
    }

    /**
     * Phương thức tiện ích: Kiểm tra 1 xâu có chứa 1 xâu con không
     * @param field xâu cần kiểm tra
     * @param search chuỗi từ khóa
     * @return true - có chứa, false - không chứa
     */
    private boolean isContain(String field, String search) {
        return field.toLowerCase().contains(search);
    }
    
    /**
     * Phương thức reload dữ liệu bind trên màn hình
     */
    private void reloadData() {
        setTableViewData();
    }
    
    /**
     * Phương thức xử lý hành động xóa
     * @param bikeType loại xe
     */
    private void deleteAction(BikeType bikeType) {
    	log.info("Delete button clicked");
    	String bikeTypeName = bikeType.getBikeTypeName();
    	
    	Alert alert = new Alert(AlertType.CONFIRMATION, 
    			String.format("Are you sure you want to delete bike type \"%s\"?", bikeTypeName));
    	
    	Optional<ButtonType> result = alert.showAndWait();
    	
    	if(result.get().equals(ButtonType.OK)) {
    		int count = controller.deleteBikeType(bikeType.getBikeTypeNo());
            if(count < 1) {
            	new Alert(AlertType.ERROR, 
            			String.format("Failed to delete bike type \"%s\"", bikeTypeName))
            	.showAndWait();
            } else {
            	new Alert(AlertType.INFORMATION, 
            			String.format("Delete bike type \"%s\" successfully.", bikeTypeName))
            	.showAndWait();
            }
            
            reloadData();
    	}
    }
    
    /**
     * Phương thức show form thêm loại xe mới
     * @throws IOException
     */
    private void showAddBikeTypeForm() throws IOException {
    	AddBikeTypeHandler addBikeTypeHandler = new AddBikeTypeHandler(stage, Configs.BIKE_TYPE_DETAIL_FORM_SCREEN_PATH);
		addBikeTypeHandler.setHomeScreenHandler(homeScreenHandler);
	    addBikeTypeHandler.setPreviousScreen(this);
	    addBikeTypeHandler.show();	
    }
    
    /**
     * Phương thức show form sửa thông tin loại xe
     * @param bikeType loại xe
     * @throws IOException
     */
    private void showEditBikeTypeForm(BikeType bikeType) throws IOException {
    	EditBikeTypeHandler editBikeTypeHandler = new EditBikeTypeHandler(stage, Configs.BIKE_TYPE_DETAIL_FORM_SCREEN_PATH, bikeType);
        editBikeTypeHandler.setHomeScreenHandler(homeScreenHandler);
        editBikeTypeHandler.setPreviousScreen(this);
        editBikeTypeHandler.show();
    }
    
    @Override
    public void forward(Map<String, Object> messages) {
    	super.forward(messages);
    	reloadData();
    }
}
