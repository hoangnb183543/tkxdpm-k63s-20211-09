package views.screen.managebiketype;

import java.io.IOException;
import java.util.Optional;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonType;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.biketype.BikeType;

@Log
public class EditBikeTypeHandler extends BikeTypeDetailHandler {
	private final String FAILURE_MESSAGE = "Failed to update bike type information.";
	private final String SUCCESS_MESSAGE = "Update bike type information successfully.";
	private final String SCREEN_TITLE = "Edit bike type";
	
    public EditBikeTypeHandler(Stage stage, String screenPath, BikeType bikeType) throws IOException {
        super(stage, screenPath, bikeType);
        setScreenTitle(SCREEN_TITLE);
    }   
    
    @Override
    protected void cancelBtnClicked(MouseEvent event) throws IOException {
    	if(hasChanged()) {
    		ButtonType buttonCancel = ButtonType.CANCEL;
    		ButtonType buttonSave = ButtonType.YES;
    		ButtonType buttonNotSave = ButtonType.NO;
    		Alert confirm = new Alert(AlertType.CONFIRMATION, "The data has been changed. Do you want to save?", buttonCancel, buttonSave, buttonNotSave);
    		Optional<ButtonType> result = confirm.showAndWait();
    		
    		if (result.get().equals(ButtonType.CANCEL)) {
    			confirm.close();
    		} else if (result.get().equals(ButtonType.YES)) {
    			saveBtnClicked(event);
    		} else if (result.get().equals(ButtonType.NO)) {
    			super.cancelBtnClicked(event);
    		}
    	} else {
        	super.cancelBtnClicked(event);
    	}
    }

	@Override
	protected void saveAction() {
		if(!hasChanged()) {
			getPreviousScreen().forward(null);
			getPreviousScreen().show();
			return;
		}
		
		String bikeTypeCode = bikeTypeCodeTxtField.getText();
		String bikeTypeName = bikeTypeNameTxtField.getText();
		String description = descriptionTxtField.getText();
		double depositAmount = Double.parseDouble(depositAmountTxtField.getText());
		String formula = getFormula();
		
		this.bikeType.setBikeTypeCode(bikeTypeCode);
		this.bikeType.setBikeTypeName(bikeTypeName);
		this.bikeType.setBikeTypeDescription(description);
		this.bikeType.setDepositAmount(depositAmount);
		this.bikeType.setFormula(formula);
		
		int count = controller.updateBikeType(this.bikeType);
		if(count < 1) {
			log.info(FAILURE_MESSAGE);
			Alert alert = new Alert(AlertType.ERROR, FAILURE_MESSAGE);
			alert.showAndWait();
		} else {
			log.info(SUCCESS_MESSAGE);
			Alert alert = new Alert(AlertType.INFORMATION, SUCCESS_MESSAGE);
			alert.showAndWait();
			getPreviousScreen().forward(null);
			getPreviousScreen().show();
		}
		
	}
}
