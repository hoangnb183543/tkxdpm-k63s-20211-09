package views.screen.payment;

import controller.PaymentController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import utils.Configs;
import utils.CurrencyUtils;
import views.screen.BaseScreenHandler;
import views.screen.bikesearch.BikeInfoScreenHandler;
import views.screen.popup.PopupScreenHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Log
public class DepositFormHandler extends BaseScreenHandler {

    @FXML
    private TextField cardHolderName;
    @FXML
    private TextField cardNumber;
    @FXML
    private TextField expDate;
    @FXML
    private TextField issueBank;
    @FXML
    private TextField transDescription;
    @FXML
    private Label amountLabel;
    @FXML
    private PasswordField securityCode;
    @FXML
    private Button backBtn;
    @FXML
    private Button confirmBtn;

    private PaymentController controller;
    private double amount;

    public DepositFormHandler(Stage stage, String screenPath, double amount) throws IOException {
        super(stage, screenPath, true);
        this.amount = amount;
        amountLabel.setText(CurrencyUtils.convertToString(amount));
        backBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                Map<String, Object> messages = new HashMap<>();
                messages.put(BikeInfoScreenHandler.IS_BACK, true);
                getPreviousScreen().forward(messages);
                getPreviousScreen().show();
            }
        });
        confirmBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                log.info("Clicked confirm button");
                Map<String, Object> params = getUserInputs();
                controller = new PaymentController(params);
                if (!controller.isValidInput(params)) {
                    try {
                        PopupScreenHandler.error("Please fill in all field");
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    try {
                        Map<String, String> message = controller.processDeposit((Long) messages.get("BIKE_NO"));
                        DepositResultHandler depositResultHandler = new DepositResultHandler(stage,
                                Configs.DEPOSIT_RESULT_SCREEN_PATH,
                                message);
                        depositResultHandler.setHomeScreenHandler(homeScreenHandler);
                        depositResultHandler.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private Map<String, Object> getUserInputs() {
        Map<String, Object> params = new HashMap<>();
        params.put(PaymentController.OWNER, cardHolderName.getText());
        params.put(PaymentController.CARD_CODE, cardNumber.getText());
        params.put(PaymentController.CVV_CODE, securityCode.getText());
        params.put(PaymentController.DATE_EXPIRED, expDate.getText());
        params.put(PaymentController.DESCRIPTION, transDescription.getText());
        params.put(PaymentController.AMOUNT, this.amount);
        return params;
    }

}
