package views.screen.payment;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

import java.io.IOException;
import java.util.Map;

public class DepositResultHandler extends BaseScreenHandler {

    @FXML
    private Label paymentResult;
    @FXML
    private Label paymentMessage;
    @FXML
    private Button homeBtn;

    public DepositResultHandler(Stage stage, String screenPath, Map<String, String> messages) throws IOException {
        super(stage, screenPath, true);
        String result = messages.get("PAYMENT_RESULT");
        String message = messages.get("PAYMENT_MESSAGE");
        paymentResult.setText(result);
        paymentMessage.setText(message);

        homeBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                homeScreenHandler.show();
            }
        });
    }

}
