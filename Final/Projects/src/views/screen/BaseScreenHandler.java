package views.screen;

import controller.BaseController;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import utils.Configs;
import views.screen.home.HomeScreenHandler;

import java.io.File;
import java.io.IOException;
import java.util.Map;

@Log
public class BaseScreenHandler extends FXMLScreenHandler {

	protected Scene scene;
	protected BaseScreenHandler prev;
	protected final Stage stage;
	protected HomeScreenHandler homeScreenHandler;
	protected Map<String, Object> messages;
	protected BaseController bController;

	@FXML
	private ImageView ecobikeImg;

	private BaseScreenHandler(String screenPath) throws IOException {
		super(screenPath);
		this.stage = new Stage();
	}

	public void setPreviousScreen(BaseScreenHandler prev) {
		this.prev = prev;
	}

	public BaseScreenHandler getPreviousScreen() {
		return this.prev;
	}

	public BaseScreenHandler(Stage stage, String screenPath) throws IOException {
		super(screenPath);
		this.stage = stage;
	}

	public BaseScreenHandler(Stage stage, String screenPath, boolean hasLogo) throws IOException {
		super(screenPath);
		this.stage = stage;
		if (hasLogo) {
			File file = new File(Configs.LOGO_PATH);
			Image im = new Image(file.toURI().toString());
			ecobikeImg.setImage(im);
			ecobikeImg.setOnMouseClicked(e -> homeScreenHandler.show());
		}
	}

	public void show() {
		if (this.scene == null) {
			this.scene = new Scene(this.content);
		}
		this.stage.setScene(this.scene);
		this.stage.show();
	}

	public void setScreenTitle(String string) {
		this.stage.setTitle(string);
	}

	public void setBController(BaseController bController){
		this.bController = bController;
	}

	public BaseController getBController(){
		return this.bController;
	}

	public void forward(Map<String, Object> messages) {
		this.messages = messages;
	}

	public void setHomeScreenHandler(HomeScreenHandler HomeScreenHandler) {
		this.homeScreenHandler = HomeScreenHandler;
	}

	public void handleBtnClicked(BaseScreenHandler handler, HomeScreenHandler prev, HomeScreenHandler home, String title) {
		handler.setPreviousScreen(prev);
        handler.setHomeScreenHandler(home);
        handler.setScreenTitle(title);
        handler.setBController(getBController());
        handler.show();
	}
    
}
