package views.screen.bikesearch;

import controller.BikeInfoController;
import controller.RentalHistoryController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import utils.Configs;
import utils.CurrencyUtils;
import utils.DateUtils;
import views.screen.BaseScreenHandler;
import views.screen.payment.DepositFormHandler;
import views.screen.popup.PopupScreenHandler;

import java.io.IOException;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Log
public class BikeInfoScreenHandler extends BaseScreenHandler {

    @FXML
    private Label bikeName;
    @FXML
    private Label bikeType;
    @FXML
    private Label weight;
    @FXML
    private Label licensePlate;
    @FXML
    private Label manuDate;
    @FXML
    private Label producer;
    @FXML
    private Label cost;
    @FXML
    private Label batteryPercentage;
    @FXML
    private Label loadCycles;
    @FXML
    private Label usageTimeRemaining;
    @FXML
    private Label batteryPercentageLabel;
    @FXML
    private Label loadCyclesLabel;
    @FXML
    private Label usageTimeRemainingLabel;
    @FXML
    private Button backBtn;
    @FXML
    private Button rentBtn;

    public static final String BIKE_NO = "BIKE_NO";
    public static final String IS_BACK = "IS_BACK";
    private final String NEXT_SCREEN_TITLE = "Deposit form";
    private final Map<String, Object> bikeInfo;
    private BikeInfoController bikeInfoController;
    private RentalHistoryController rentalHistoryController;

    public BikeInfoScreenHandler(Stage stage, String screenPath, Map<String, Object> messages) throws IOException {
        super(stage, screenPath, true);
        this.bikeInfoController = new BikeInfoController();
        this.rentalHistoryController = new RentalHistoryController();
        this.bikeInfo = setBikeInfo(messages);
        setUpBtnClicked(stage);
    }

    private void setUpBtnClicked(Stage stage) {
        backBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                getPreviousScreen().show();
            }
        });
        BaseScreenHandler prev = this;
        rentBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                try {
                    long bikeNo = (Long) bikeInfo.get(BikeInfoController.BIKE_NO);
                    if (rentalHistoryController.isRenting()) {
                        PopupScreenHandler.error("You are renting another bike. Return it to rent this one...");
                    } else if (!bikeInfoController.isAvailable(bikeNo)) {
                        PopupScreenHandler.error("This bike is not available to rent");
                    } else {
                        try {
                            Map<String, Object> messages = new HashMap<>();
                            messages.put(BikeInfoController.BIKE_NO, bikeNo);

                            bikeInfoController.updateBikeStatusInProgress(bikeNo);

                            DepositFormHandler depositFormHandler = new DepositFormHandler(stage,
                                    Configs.DEPOSIT_FORM_SCREEN_PATH,
                                    bikeInfoController.getDepositAmtByBikeNo((Long) bikeInfo.get(BikeInfoController.BIKE_NO)));
                            depositFormHandler.setPreviousScreen(prev);
                            depositFormHandler.setHomeScreenHandler(homeScreenHandler);
                            depositFormHandler.setScreenTitle(NEXT_SCREEN_TITLE);
                            depositFormHandler.forward(messages);
                            depositFormHandler.show();
                        } catch (Exception e) {
                            bikeInfoController.updateBikeStatusAvailable((Long) bikeInfo.get(BikeInfoController.BIKE_NO));
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private Map<String, Object> setBikeInfo(Map<String, Object> messages) {
        Long bikeNo = (Long) messages.get(BIKE_NO);
        if (bikeNo != null) {
            Map<String, Object> bikeInfo = bikeInfoController.getBikeInfo(bikeNo);
            try {
                bikeName.setText((String) bikeInfo.get(BikeInfoController.BIKE_NAME));
                bikeType.setText((String) bikeInfo.get(BikeInfoController.BIKE_TYPE));
                weight.setText(String.valueOf(bikeInfo.get(BikeInfoController.WEIGHT)));
                licensePlate.setText((String) bikeInfo.get(BikeInfoController.LICENSE_PLATE));
                manuDate.setText(DateUtils.convertDateToString((LocalDate) bikeInfo.get(BikeInfoController.MANU_DATE)));
                producer.setText((String) bikeInfo.get(BikeInfoController.PRODUCER));
                cost.setText(CurrencyUtils.convertToString((Integer) bikeInfo.get(BikeInfoController.COST)));
                if (bikeInfo.get(BikeInfoController.BATTERY_PERCENTAGE) != null) {
                    batteryPercentageLabel.setVisible(true);
                    batteryPercentage.setVisible(true);
                    batteryPercentage.setText(String.valueOf(bikeInfo.get(BikeInfoController.BATTERY_PERCENTAGE)));
                }

                if (bikeInfo.get(BikeInfoController.LOAD_CYCLES) != null) {
                    loadCyclesLabel.setVisible(true);
                    loadCycles.setVisible(true);
                    loadCycles.setText(String.valueOf(bikeInfo.get(BikeInfoController.LOAD_CYCLES)));
                }

                if (bikeInfo.get(BikeInfoController.USAGE_TIME_REMAINING_IN_SECOND) != null) {
                    usageTimeRemainingLabel.setVisible(true);
                    usageTimeRemaining.setVisible(true);
                    usageTimeRemaining.setText(String.valueOf(bikeInfo.get(BikeInfoController.USAGE_TIME_REMAINING_IN_SECOND)));
                }

                return bikeInfo;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    @Override
    public void show() {
        try {
            boolean isBack = (Boolean) this.messages.get(IS_BACK);
            if (isBack) {
                bikeInfoController.updateBikeStatusAvailable((Long) bikeInfo.get(BikeInfoController.BIKE_NO));
            }
        } catch (Exception e) {
            log.info("Not back");
        }
        if (this.scene == null) {
            this.scene = new Scene(this.content);
        }
        this.stage.setScene(this.scene);
        this.stage.show();
    }

}
