package exception;

public class InvalidCardException extends EcoBikeException {

    public InvalidCardException(String message) {
        super(message);
    }

}
