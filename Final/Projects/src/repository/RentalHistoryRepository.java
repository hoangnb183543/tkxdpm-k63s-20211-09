package repository;

import model.rentalhistory.BikeRentalHistory;

import java.util.List;

public interface RentalHistoryRepository {

    List<BikeRentalHistory> findHistoriesByUserNo(long userNo);

    BikeRentalHistory save(BikeRentalHistory history);

    List<BikeRentalHistory> findBikeIsRenting();
}
