package repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lombok.extern.java.Log;
import model.biketype.BikeType;
import model.dbconnection.SQLLiteConnection;
import repository.IBikeTypeRepository;

public class BikeTypeRepositoryImpl implements IBikeTypeRepository {

	@Override
	public List<BikeType> getAllBikeTypes() {
		String query = "SELECT * FROM bike_type";
		try {
            Statement statement = SQLLiteConnection.getConnection().createStatement();
            ResultSet res = statement.executeQuery(query);
            List<BikeType> bikeTypes = new ArrayList<>();
            while (res.next()) {
            	bikeTypes.add(BikeType.builder()
                        .bikeTypeNo(res.getLong("BIKE_TYPE_NO"))
                        .bikeTypeCode(res.getString("BIKE_TYPE_CODE"))
                        .bikeTypeName(res.getString("BIKE_TYPE_NAME"))
                        .bikeTypeDescription(res.getString("DESCRIPTION"))
                        .depositAmount(res.getDouble("DEPOSIT_AMT"))
                        .formula(res.getString("FORMULA"))
                        .build());
            }
            return bikeTypes;
        } catch (Exception e) {
            e.printStackTrace();
        }
		
        return null;
	}

	@Override
	public BikeType getBikeTypeByNo(long bikeTypeNo) {
		String query = "SELECT * FROM bike_type WHERE bike_type_no = ?";
		try {
			Connection conn = SQLLiteConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(query);
			ps.setLong(1, bikeTypeNo);

            ResultSet res = ps.executeQuery();
            if (res.next()) {
            	return BikeType.builder()
                        .bikeTypeNo(res.getLong("BIKE_TYPE_NO"))
                        .bikeTypeCode(res.getString("BIKE_TYPE_CODE"))
                        .bikeTypeName(res.getString("BIKE_TYPE_NAME"))
                        .bikeTypeDescription(res.getString("DESCRIPTION"))
                        .depositAmount(res.getDouble("DEPOSIT_AMT"))
                        .formula(res.getString("FORMULA"))
                        .build();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		return null;
	}

	@Override
	public int addBikeType(BikeType bikeType) {
		String sql = "INSERT INTO bike_type(bike_type_code, bike_type_name, description, deposit_amt, formula) VALUES(?, ?, ?, ?, ?)";
		try {
			Connection conn = SQLLiteConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, bikeType.getBikeTypeCode());
			ps.setString(2, bikeType.getBikeTypeName());
			ps.setString(3, bikeType.getBikeTypeDescription());
			ps.setDouble(4, bikeType.getDepositAmount());
			ps.setString(5, bikeType.getFormula());
			
            int count = ps.executeUpdate();
            
            return count;
        } catch (Exception e) {
            e.printStackTrace();
        }
		return 0;
	}

	@Override
	public int updateBikeType(BikeType bikeType) {
		String sql = "UPDATE bike_type SET bike_type_code = ?, bike_type_name = ?, description = ?, deposit_amt = ?, formula = ? WHERE bike_type_no = ?";
		try {
			Connection conn = SQLLiteConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, bikeType.getBikeTypeCode());
			ps.setString(2, bikeType.getBikeTypeName());
			ps.setString(3, bikeType.getBikeTypeDescription());
			ps.setDouble(4, bikeType.getDepositAmount());
			ps.setString(5, bikeType.getFormula());
			ps.setLong(6, bikeType.getBikeTypeNo());
			
            int count = ps.executeUpdate();
            
            return count;
        } catch (Exception e) {
            e.printStackTrace();
        }
		return 0;
	}

	@Override
	public int deleteBikeTypeByNo(long bikeTypeNo) {
		String sql = "DELETE FROM bike_type WHERE bike_type_no = ?";
		try {
			Connection conn = SQLLiteConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, bikeTypeNo);
			
            int count = ps.executeUpdate();
            
            return count;
        } catch (Exception e) {
            e.printStackTrace();
        }
		return 0;
	}
	
}
