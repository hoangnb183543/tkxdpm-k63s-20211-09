package controller;

import lombok.extern.java.Log;
import model.bike.Bike;
import model.biketype.BikeType;
import repository.BikeRepository;
import repository.IBikeTypeRepository;
import repository.impl.BikeRepositoryImpl;
import repository.impl.BikeTypeRepositoryImpl;
import utils.rentalprice.RentalPriceConstant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Lớp controller Loại xe
 * 
 * @author PTHIEU 01.01.2022
 */
@Log
public class BikeTypeController extends BaseController {
	
	public static final String BIKE_TYPE_CODE = "BIKE_TYPE_CODE";
    public static final String BIKE_TYPE_NAME = "BIKE_TYPE_NAME";
    public static final String BIKE_TYPE_DESCRIPTION = "BIKE_TYPE_DESCRIPTION";
    public static final String BIKE_TYPE_DEPOSIT_AMOUNT = "BIKE_TYPE_DEPOSIT_AMOUNT";

    private IBikeTypeRepository bikeTypeRepository;

    public BikeTypeController() {
        this.bikeTypeRepository = new BikeTypeRepositoryImpl();
    }

    public BikeTypeController(IBikeTypeRepository bikeTypeRepository) {
        this.bikeTypeRepository = bikeTypeRepository;
    }

    /**
     * Phương thức lấy ra danh sách tất cả loại xe
     * @return Danh sách loại xe
     */
    public List<BikeType> getAllBikeTypes() {
        log.info("Start find all bike types");
        return bikeTypeRepository.getAllBikeTypes();
    }

    /**
     * Phương thức lấy thông tin Loại xe theo id
     * @param bikeTypeNo id loại xe
     * @return Thông tin loại xe
     */
    public BikeType getBikeTypeByNo(long bikeTypeNo) {
        log.info(String.format("Start find bike type no %d", bikeTypeNo));
        BikeType bikeType = bikeTypeRepository.getBikeTypeByNo(bikeTypeNo);
        if (bikeType == null) {
            log.info("Bike type not found...");
        }
        return bikeType;
    }
    
    /**
     * Phương thức thêm loại xe mới
     * @param bikeType Thông tin loại xe cần thêm
     * @return số bản ghi được thêm
     */
    public int addBikeType(BikeType bikeType) {
        log.info(String.format("Start add bike type: %s", bikeType.toString()));
        int count = bikeTypeRepository.addBikeType(bikeType);
        if (count < 1) {
            log.info("Add bike type fail.");
        }
        return count;
    }
    
    /**
     * Phương thức cập nhật thông tin loại xe theo id
     * @param bikeType Thông tin loại xe cần cập nhật
     * @return số bản ghi bị ảnh hưởng
     */
    public int updateBikeType(BikeType bikeType) {
        log.info(String.format("Start update bike type: %s", bikeType.toString()));
        int count = bikeTypeRepository.updateBikeType(bikeType);
        if (count < 1) {
            log.info("Update bike type fail.");
        }
        return count;
    }
    
    /**
     * Phương thức xóa loại xe theo id
     * @param bikeTypeNo id loại xe
     * @return số bản ghi bị xóa
     */
    public int deleteBikeType(long bikeTypeNo) {
        log.info(String.format("Start delete bike type no: %d", bikeTypeNo));
        int count = bikeTypeRepository.deleteBikeTypeByNo(bikeTypeNo);
        if (count < 1) {
            log.info("Delete bike type fail.");
        }
        return count;
    }
    
    /**
     * Validate thông tin input
     * @param bikeTypeInfo dữ liệu input loại xe
     * @return true - thỏa mãn, false - không thỏa mãn
     */
    public boolean validateInfo(Map<String, Object> bikeTypeInfo) {
    	try {
    		String bikeTypeCode = (String)bikeTypeInfo.get(BIKE_TYPE_CODE);
    		String bikeTypeName = (String)bikeTypeInfo.get(BIKE_TYPE_NAME);
    		String bikeTypeDepositAmount = (String)bikeTypeInfo.get(BIKE_TYPE_DEPOSIT_AMOUNT);
    		
    		if(bikeTypeCode == null || bikeTypeCode.isBlank()) {
    			return false;
    		}
    		
    		if(bikeTypeName == null || bikeTypeName.isBlank()) {
    			return false;
    		}
    		
    		if(bikeTypeDepositAmount == null || bikeTypeDepositAmount.isBlank()) {
    			return false;
    		}
    		
			double depositAmt = Double.parseDouble(bikeTypeDepositAmount);
			if(depositAmt < 0) {
				return false;
			}
    	} catch (Exception ex) {
    		return false;
    	}
    	
    	return validateInfoFormula(bikeTypeInfo);
    }
    
    /**
     * Validate thông tin các ô thiết lập giá thuê xe
     * @param bikeTypeInfo dữ liệu input loại xe
     * @return true - thỏa mãn, false - không thỏa mãn
     */
    private boolean validateInfoFormula(Map<String, Object> bikeTypeInfo) {
    	try {
    		String freeMinutesDuration = (String)bikeTypeInfo.get(RentalPriceConstant.FREE_MINUTES_DURATION);
    		String first30MinutesPrice = (String)bikeTypeInfo.get(RentalPriceConstant.FIRST_30_MINUTES_PRICE);
    		String every15MinutesNextPrice = (String)bikeTypeInfo.get(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE);
    		
    		if(freeMinutesDuration == null || freeMinutesDuration.isBlank()) {
    			return false;
    		}
    		
    		if(first30MinutesPrice == null || first30MinutesPrice.isBlank()) {
    			return false;
    		}
    		
    		if(every15MinutesNextPrice == null || every15MinutesNextPrice.isBlank()) {
    			return false;
    		}
    		
			if(Integer.parseInt(freeMinutesDuration) < 0) {
				return false;
			}
			
			if(Double.parseDouble(first30MinutesPrice) < 0) {
				return false;
			}
			
			if(Double.parseDouble(every15MinutesNextPrice) < 0) {
				return false;
			}
    	} catch (Exception ex) {
    		return false;
    	}
    	return true;
    }
}
