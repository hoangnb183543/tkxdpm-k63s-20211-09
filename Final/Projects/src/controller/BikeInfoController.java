package controller;

import model.bike.Bike;
import model.bike.BikeStatus;
import model.bike.ElectricBike;
import repository.BikeRepository;
import repository.impl.BikeRepositoryImpl;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class BikeInfoController extends BaseController {

    public static final String BIKE_NO = "BIKE_NO";
    public static final String BIKE_NAME = "BIKE_NAME";
    public static final String BIKE_CODE = "BIKE_CODE";
    public static final String WEIGHT = "WEIGHT";
    public static final String BIKE_TYPE = "BIKE_TYPE";
    public static final String LICENSE_PLATE = "LICENSE_PLATE";
    public static final String MANU_DATE = "MANU_DATE";
    public static final String PRODUCER = "PRODUCER";
    public static final String COST = "COST";
    public static final String STATUS = "STATUS";
    public static final String BATTERY_PERCENTAGE = "BATTERY_PERCENTAGE";
    public static final String LOAD_CYCLES = "LOAD_CYCLES";
    public static final String USAGE_TIME_REMAINING_IN_SECOND = "USAGE_TIME_REMAINING_IN_SECOND";

    BikeRepository bikeRepository;

    public BikeInfoController() {
        bikeRepository = new BikeRepositoryImpl();
    }

    public double getDepositAmtByBikeNo(long bikeNo) {
        Double amt = bikeRepository.getDepositAmtByBikeNo(bikeNo);
        return Objects.requireNonNullElse(amt, 0.0);
    }

    public void updateBikeStatusRented(long bikeNo) {
        bikeRepository.updateBikeStatus(bikeNo, BikeStatus.RENTED);
    }

    public void updateBikeStatusAvailable(long bikeNo) {
        bikeRepository.updateBikeStatus(bikeNo, BikeStatus.AVAILABLE);
    }

    public void updateBikeStatusInProgress(long bikeNo) {
        bikeRepository.updateBikeStatus(bikeNo, BikeStatus.RENTING_IN_PROGRESS);
    }

    public Map<String, Object> getBikeInfo(long bikeNo) {
        Map<String, Object> results = new HashMap<>();
        Bike bike = bikeRepository.findBikeByNo(bikeNo);
        if (bike != null) {
            results.put(BIKE_NO, bike.getBikeNo());
            results.put(BIKE_NAME, bike.getName());
            results.put(BIKE_TYPE, bike.getType());
            results.put(WEIGHT, bike.getWeight());
            results.put(LICENSE_PLATE, bike.getLicensePlate());
            results.put(MANU_DATE, bike.getManufacturingDate());
            results.put(PRODUCER, bike.getProducer());
            results.put(COST, bike.getCost());
            results.put(STATUS, bike.getStatus().getName());
            results.put(BIKE_CODE, bike.getBikeCode());
            if (bike instanceof ElectricBike) {
                ElectricBike eBike = (ElectricBike) bike;
                results.put(USAGE_TIME_REMAINING_IN_SECOND, eBike.getUsageTimeRemainingInSecond());
                results.put(LOAD_CYCLES, eBike.getLoadCycles());
                results.put(BATTERY_PERCENTAGE, eBike.getBatteryPercentage());
            }
        }
        return results;
    }

    public boolean isAvailable(long bikeNo) {
        Bike bike = bikeRepository.findBikeByNo(bikeNo);
        if (bike != null) {
            return bike.getStatus() == BikeStatus.AVAILABLE;
        }
        return false;
    }

}
