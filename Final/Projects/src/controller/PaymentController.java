package controller;

import exception.InvalidCardException;
import lombok.extern.java.Log;
import model.payment.PaymentCard;
import model.payment.PaymentCode;
import model.payment.PaymentTransaction;
import paymentsubsystem.PaymentSubSystem;
import paymentsubsystem.impl.PaymentSubSystemImpl;
import repository.PaymentTransactionRepository;
import repository.impl.PaymentTransactionRepositoryImpl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Log
public class PaymentController extends BaseController {

    PaymentSubSystem paymentSubSystem;
    Map<String, Object> params;
    PaymentTransactionRepository transactionRepository;
    BikeInfoController bikeInfoController;
    RentalHistoryController rentalHistoryController;

    public static final String OWNER = "OWNER";
    public static final String CARD_CODE = "CARD_CODE";
    public static final String DATE_EXPIRED = "DATE_EXPIRED";
    public static final String CVV_CODE = "CVV_CODE";
    public static final String AMOUNT = "AMOUNT";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String PAYMENT_RESULT = "PAYMENT_RESULT";
    public static final String PAYMENT_MESSAGE = "PAYMENT_MESSAGE";

    private static final String SLASH = "/";
    private static final List<Integer> MONTHS_HAVE_31_DAYS = List.of(1, 3, 5, 7, 8, 10, 12);

    public PaymentController() {
        this.paymentSubSystem = new PaymentSubSystemImpl();
        this.transactionRepository = new PaymentTransactionRepositoryImpl();
        this.bikeInfoController = new BikeInfoController();
        this.rentalHistoryController = new RentalHistoryController();
    }

    public PaymentController(Map<String, Object> params) {
        this();
        this.params = params;
    }

    public Map<String, String> processDeposit(long bikeNo) {
        log.info(String.format("Bike no = %s", bikeNo));
        Map<String, String> result = new HashMap<>();
        result.put(PAYMENT_RESULT, "PAYMENT FAILED");
        try {
            String owner = (String) params.get(OWNER);
            String cardCode = (String) params.get(CARD_CODE);
            String dateExpired = (String) params.get(DATE_EXPIRED);
            String cvvCode = (String) params.get(CVV_CODE);
            String description = (String) params.get(DESCRIPTION);
            double amount = (Double) params.get(AMOUNT);

            PaymentCard paymentCard = PaymentCard.builder()
                    .owner(owner)
                    .cardCode(cardCode)
                    .cvvCode(cvvCode)
                    .dateExpired(parseDateExpired(dateExpired))
                    .build();
            PaymentTransaction transaction = paymentSubSystem.payOrder(paymentCard, amount, description);
            transactionRepository.save(transaction);
            if (transaction.getPaymentCode() == PaymentCode.SUCCESS) {
                result.put(PAYMENT_RESULT, "PAYMENT SUCCESSFUL");
                bikeInfoController.updateBikeStatusRented(bikeNo);
                rentalHistoryController.saveRentHistory(bikeNo);
            } else {
                bikeInfoController.updateBikeStatusAvailable(bikeNo);
            }
            result.put(PAYMENT_MESSAGE, transaction.getPaymentCode().getDescription());
        } catch (Exception e) {
            bikeInfoController.updateBikeStatusAvailable(bikeNo);
            result.put(PAYMENT_MESSAGE, e.getMessage());
        }

        return result;
    }

    public boolean isValidInput(Map<String, Object> params) {
        try {
            String owner = (String) params.get(OWNER);
            String cardCode = (String) params.get(CARD_CODE);
            String dateExpired = (String) params.get(DATE_EXPIRED);
            String cvvCode = (String) params.get(CVV_CODE);
            String description = (String) params.get(DESCRIPTION);
            double amount = (Double) params.get(AMOUNT);
            return !(isNullOrEmpty(owner) || isNullOrEmpty(cardCode)
                    || isNullOrEmpty(cvvCode) || isNullOrEmpty(dateExpired)
                    || isNullOrEmpty(description));
        } catch (Exception e) {
            return false;
        }
    }

    public String parseDateExpired(String dateStr) {
        log.info("Start validate date expired " + dateStr);
        String errorMessage = "Error when parse date expired";

        if (isNullOrEmpty(dateStr)) {
            throw new InvalidCardException(errorMessage);
        }

        List<String> stringList = List.of(dateStr.split(SLASH));
        if (stringList.size() != 2) {
            throw new InvalidCardException(errorMessage);
        }

        try {
            int day = Integer.parseInt(stringList.get(0));
            int month = Integer.parseInt(stringList.get(1));
            if (!isValidDateMonth(day, month)) {
                throw new InvalidCardException(errorMessage);
            }
            return String.valueOf(month) + day;
        } catch (Exception e) {
            throw new InvalidCardException(errorMessage);
        }
    }

    private boolean isValidDateMonth(int day, int month) {
        return (month == 2 && (day <= 29 && day >= 1))
                || (MONTHS_HAVE_31_DAYS.contains(month) && (day <= 31 && day >= 1))
                || (month != 2 && day <= 30 && day >= 1);
    }

    private boolean isNullOrEmpty(String string) {
        return (string == null) || string.length() == 0 || string.trim().length() == 0;
    }

}
