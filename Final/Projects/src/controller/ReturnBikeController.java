package controller;

import repository.RentalHistoryRepository;
import repository.DockingStationRepository;
import repository.impl.RentalHistoryRepositoryImpl;
import repository.impl.DockingStationRepositoryImpl;

import java.util.List;

import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import model.rentalhistory.BikeRentalHistory;
import model.rentalhistory.UserAction;

@Log
public class ReturnBikeController extends BaseController {
	 	RentalHistoryRepository historyRepository;
	 	DockingStationRepository dockingStationRepository;
	 
	    public ReturnBikeController() {
	        historyRepository = new RentalHistoryRepositoryImpl();
	        dockingStationRepository = new DockingStationRepositoryImpl();
	    }
	    
	    public BikeRentalHistory getLastHistory() {
	    	List<BikeRentalHistory> histories = historyRepository.findHistoriesByUserNo(getCurrentUserNo());
	    	BikeRentalHistory currnetHistory = histories.get(histories.size() - 1);
	    	return currnetHistory;
	    }
	    
	    public boolean isRenting() {
	    	BikeRentalHistory currnetHistory = getLastHistory();
    		return currnetHistory.getUserAction() == UserAction.RENT;
	    }
	    
	    public List<DockingStation> findListDockingStation() {
	    	List<DockingStation> stations = dockingStationRepository.findAllDockingStations();
	    	stations.removeIf(station -> station.getCapacity() < 1);
	    	return stations;
	    }
}
