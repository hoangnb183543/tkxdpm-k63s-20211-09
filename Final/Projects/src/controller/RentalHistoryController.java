package controller;

import exception.EcoBikeException;
import model.bike.BikeStatus;
import model.rentalhistory.BikeRentalHistory;
import model.rentalhistory.UserAction;
import repository.BikeRepository;
import repository.RentalHistoryRepository;
import repository.impl.BikeRepositoryImpl;
import repository.impl.RentalHistoryRepositoryImpl;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.Optional;

public class RentalHistoryController extends BaseController {

    RentalHistoryRepository historyRepository;
    BikeRepository bikeRepository;

    public RentalHistoryController() {
        historyRepository = new RentalHistoryRepositoryImpl();
        bikeRepository = new BikeRepositoryImpl();
    }

    public BikeRentalHistory saveRentHistory(long bikeNo) {
        long userNo = getCurrentUserNo();

        bikeRepository.updateBikeStatus(bikeNo, BikeStatus.RENTED);
        return historyRepository.save(BikeRentalHistory.builder()
                        .bikeNo(bikeNo)
                        .userNo(userNo)
                        .createdOn(LocalDateTime.now())
                        .userAction(UserAction.RENT)
                        .build());
    }

    public BikeRentalHistory saveReturnHistory(long bikeNo) {
        long userNo = getCurrentUserNo();

        bikeRepository.updateBikeStatus(bikeNo, BikeStatus.AVAILABLE);
        return historyRepository.save(BikeRentalHistory.builder()
                        .bikeNo(bikeNo)
                        .userNo(userNo)
                        .createdOn(LocalDateTime.now())
                        .userAction(UserAction.RETURN)
                        .build());
    }

    public boolean isRenting() {
        long userNo = getCurrentUserNo();
        Optional<BikeRentalHistory> lastHistory = historyRepository.findHistoriesByUserNo(userNo).stream()
                .max(Comparator.comparing(BikeRentalHistory::getCreatedOn));
        return lastHistory.isPresent() && lastHistory.get().getUserAction() == UserAction.RENT;
    }

}
