package utils;

public class Configs {

    // static resource
    public static final String IMAGE_PATH = "assets/images";
    public static final String SPLASH_SCREEN_PATH = "/views/fxml/splash.fxml";
    public static final String HOME_SCREEN_PATH = "/views/fxml/home.fxml";
    public static final String DEPOSIT_FORM_SCREEN_PATH = "/views/fxml/deposit-form.fxml";
    public static final String DEPOSIT_RESULT_SCREEN_PATH = "/views/fxml/deposit-result.fxml";
    public static final String VIEW_BIKE_INFO_SCREEN_PATH = "/views/fxml/bike-info.fxml";
    public static final String MANAGE_BIKE_TYPE_SCREEN_PATH = "/views/fxml/manage-bike-type.fxml";
    public static final String BIKE_TYPE_DETAIL_FORM_SCREEN_PATH = "/views/fxml/bike-type-detail-form.fxml";
    public static final String POPUP_PATH = "/views/fxml/popup.fxml";
    public static final String SEARCH_BIKE_SCREEN_PATH = "/views/fxml/search-bike.fxml";
    public static final String SEARCH_DOCKING_STATION_SCREEN_PATH = "/views/fxml/search-docking-station-screen.fxml";
    public static final String SEARCH_DOCKING_STATION_FORM_PATH = "/views/fxml/search-docking-station-form.fxml";
    public static final String PARKING_MANAGER_PATH = "/views/fxml/parking-manager.fxml";
    public static final String ADD_PARKING_SCREEN_PATH = "/views/fxml/add-parking.fxml";
    public static final String RESULT_ADD_PARKING_SCREEN_PATH = "/views/fxml/result_addparking.fxml";
    public static final String CHOOSE_BIKE_TO_RETURN = "/views/fxml/ChoseBikeToReturn.fxml";
    public static final String REFUND_PATH = "/views/fxml/RefundScreen.fxml";
    public static final String REFUND_RESULT_PATH = "/views/fxml/RefundResult.fxml";
    public static final String NOT_FOUND_BIKE_PATH = "/views/fxml/NotFoundBike.fxml";
  
    public static final String LOGO_PATH = "assets/images/logo.png";
    public static final String ICON_PATH = "assets/images/icon.png";
}
