package utils;

import java.text.NumberFormat;
import java.util.Locale;

public class CurrencyUtils {

    public static String convertToString(double amount) {
        Locale locale = new Locale("vi", "VN");
        NumberFormat format = NumberFormat.getCurrencyInstance(locale);
        return format.format(amount);
    }

}
