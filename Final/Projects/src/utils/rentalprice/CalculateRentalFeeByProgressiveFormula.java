package utils.rentalprice;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import controller.BikeInfoController;
import controller.BikeTypeController;
import exception.InvalidFormulaCaculatingRentalException;
import lombok.extern.java.Log;
import model.biketype.BikeType;
import repository.IBikeTypeRepository;
import repository.impl.BikeTypeRepositoryImpl;
import utils.CurrencyUtils;

/**
 * Lá»›p tÃ­nh tiá»�n thuÃª xe theo cÃ´ng thá»©c lÅ©y tiáº¿n
 * 
 * @author PTHIEU 06.01.2022
 */
@Log
public class CalculateRentalFeeByProgressiveFormula extends CalculateRentalFee {
	
	public CalculateRentalFeeByProgressiveFormula(IBikeTypeRepository bikeTypeRepository, long bikeTypeNo) {
		super(bikeTypeRepository, bikeTypeNo);
	}
	
	public CalculateRentalFeeByProgressiveFormula(long bikeTypeNo) {
		super(bikeTypeNo);
	}
	

	@Override
	public double getRentalPrice(long duration) throws InvalidFormulaCaculatingRentalException {
		try {
			Gson gson = new Gson();
			ProgressiveFormula progressiveFormula = gson.fromJson(this.bikeType.getFormula(), ProgressiveFormula.class); 
			long minute = (long)Math.ceil(1.0*duration / 60);
			double money = 0;
			
			if(minute <= progressiveFormula.getFreeMinutesDuration()) {
				money = 0;
			} else if (minute <= 30) {
				money = progressiveFormula.getFirst30MinutesPrice();
			} else {
				minute -= 30;
				long multiplier = (long)Math.ceil(1.0*minute / 15);
				money = progressiveFormula.getFirst30MinutesPrice() + multiplier*progressiveFormula.getEvery15MinutesNextPrice();
			}
			
			double depositAmt = this.bikeType.getDepositAmount();
			return depositAmt > money ? money : depositAmt;
		} catch (JsonSyntaxException ex) {
			throw new InvalidFormulaCaculatingRentalException("Can not convert formula to ProgressiveFormula");
		} 
	}

	@Override
	public String getRentalPriceDescription() throws InvalidFormulaCaculatingRentalException {
		try {
			Gson gson = new Gson();
			ProgressiveFormula progressiveFormula = gson.fromJson(this.bikeType.getFormula(), ProgressiveFormula.class); 
			
			String str = "Free for less than %s minute(s). After that, the rental fee is calculated incrementally. The starting price for the first 30 minutes is %s. Every next 15 minutes, pay an additional %s.";
			String freeMinutesDuration = String.valueOf(progressiveFormula.getFreeMinutesDuration());
			String first30MinutesPrice = CurrencyUtils.convertToString(progressiveFormula.getFirst30MinutesPrice());
			String every15MinutesNextPrice = CurrencyUtils.convertToString(progressiveFormula.getEvery15MinutesNextPrice());
			
			return String.format(str, freeMinutesDuration, first30MinutesPrice, every15MinutesNextPrice);
		} catch (JsonSyntaxException ex) {
			throw new InvalidFormulaCaculatingRentalException("Can not convert formula to ProgressiveFormula");
		} 
	}
	
}
