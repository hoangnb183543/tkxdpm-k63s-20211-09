package utils.rentalprice;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import exception.InvalidFormulaCaculatingRentalException;
import model.biketype.BikeType;
import repository.IBikeTypeRepository;
import repository.impl.BikeTypeRepositoryImpl;

/**
 * Lớp trừu tượng cung cấp phương thức tính số tiền thuê xe
 * 
 * @author PTHIEU 06.01.2022
 */
public abstract class CalculateRentalFee {
	
	protected IBikeTypeRepository bikeTypeRepository;
	protected BikeType bikeType;
	
	public CalculateRentalFee(IBikeTypeRepository bikeTypeRepository, long bikeTypeNo) {
		this.bikeTypeRepository = bikeTypeRepository;
		this.bikeType = this.bikeTypeRepository.getBikeTypeByNo(bikeTypeNo);
	}
	
	public CalculateRentalFee(long bikeTypeNo) {
		this.bikeTypeRepository = new BikeTypeRepositoryImpl();
		this.bikeType = this.bikeTypeRepository.getBikeTypeByNo(bikeTypeNo);
	}
	
	/**
	 * Phương thức lấy ra số tiền trả lại
	 * @param duration thời gian thuê
	 * @return số tiền trả lại
	 * @throws InvalidFormulaCaculatingRentalException
	 */
	public double getRefund(long duration) throws InvalidFormulaCaculatingRentalException {
		double depositAmt = this.bikeType.getDepositAmount();
		double rentalFee = getRentalPrice(duration);
		if(rentalFee >= depositAmt) {
			return 0;
		} else {
			return depositAmt - rentalFee;
		}
	}
	
	/**
	 * Phương thức tính số tiền thuê xe theo công thức
	 * @param duration thời gian thuê
	 * @return số tiền thuê xe
	 * @throws InvalidFormulaCaculatingRentalException
	 */
	public abstract double getRentalPrice(long duration) throws InvalidFormulaCaculatingRentalException;
	
	/**
	 * Phương thức lấy ra câu mô tả cách tính tiền
	 * @param duration thời gian thuê
	 * @return câu mô tả cách tính tiền
	 * @throws InvalidFormulaCaculatingRentalException
	 */
	public abstract String getRentalPriceDescription() throws InvalidFormulaCaculatingRentalException;
}
