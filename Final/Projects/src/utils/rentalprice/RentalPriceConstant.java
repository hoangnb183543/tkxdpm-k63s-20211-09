package utils.rentalprice;

public class RentalPriceConstant {
	public static final String FREE_MINUTES_DURATION = "freeMinutesDuration";
	public static final String FIRST_30_MINUTES_PRICE = "first30MinutesPrice";
	public static final String EVERY_15_MINUTES_NEXT_PRICE = "every15MinutesNextPrice";
}
