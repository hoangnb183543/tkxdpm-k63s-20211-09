package utils.customcontrol;

import javafx.event.EventHandler;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

public class IntegerTextField extends TextField {
	   public IntegerTextField() {
	      super();

	      addEventFilter(KeyEvent.KEY_TYPED, new EventHandler<KeyEvent>() {
	         @Override
	         public void handle(KeyEvent event) {
	            if (!isValid(getText())) {
	               event.consume();
	            }
	         }
	      });

	      textProperty().addListener((observableValue, oldValue, newValue) -> {
	            if(!isValid(newValue)) {
	               setText(oldValue);
	            }
	      });
	   }

	   private boolean isValid(final String value) {
	      if (value.length() == 0 || value.equals("-")) {
	         return true;
	      }

	      try {
	         Integer.parseInt(value);
	         return true;
	      } catch (NumberFormatException ex) {
	         return false;
	      }
	   }

	   public int getInt() {
	      try {
	         return Integer.parseInt(getText());
	      }
	      catch (NumberFormatException e) {
	         e.printStackTrace();
	         return 0;
	      }
	   }
	}