package quocpha;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import controller.RefundController;

public class RefundControllerTest {
	private RefundController controller;
	
    @Before
    public void setUp() {
        controller = new RefundController();
    }
    
    @Test
    public void InputValid() {
        Map<String, Object> params = new HashMap<>();
        params.put(RefundController.ISSUE_BANK, "Techcombank");
        params.put(RefundController.CARD_CODE, "Nhom9_QuocPham20183616");
        Assert.assertTrue(controller.isValidInput(params));

        List<String> keys = List.of(RefundController.ISSUE_BANK, RefundController.CARD_CODE);
        keys.forEach(k -> testInvalidDataTypeOrNull(k, params));
    }
    
    @Test
    public void InvalidDataTypeOrNull() {
    	Map<String, Object> params = new HashMap<>();
        List<String> keys = List.of(RefundController.ISSUE_BANK, RefundController.CARD_CODE);
        keys.forEach(k -> testInvalidDataTypeOrNull(k, params));
    }
    
    private void testInvalidDataTypeOrNull(String key, Map<String, Object> params) {
        params.put(key, null);
        Assert.assertFalse(controller.isValidInput(params));

        params.put(key, 123);
        Assert.assertFalse(controller.isValidInput(params));
    }
}
