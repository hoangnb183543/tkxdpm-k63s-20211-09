package hieupt;

import static org.junit.jupiter.api.Assertions.*;

import java.util.List;
import java.util.stream.Stream;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.CsvSource;

import utils.rentalprice.CalculateRentalFee;
import utils.rentalprice.CalculateRentalFeeByProgressiveFormula;

class CalculateRentalFeeTest {
	
	CalculateRentalFee calculateRentalFee;

	@BeforeEach
	void setUp() throws Exception {
		calculateRentalFee = new CalculateRentalFeeByProgressiveFormula(1);
	}

	@ParameterizedTest
	@CsvSource({
		"480, 0.0",
		"600, 0.0",
		"601, 10000.0",
		"1500, 10000.0",
		"1800, 10000.0",
		"1801, 13000.0",
		"3610, 19000.0"
	})
	@DisplayName("validate getRentalPrice")
	void testGetRentalPrice(long duration, double expected) {
		double fee = calculateRentalFee.getRentalPrice(duration);
		assertEquals(expected, fee);
	}

	@Test
	@DisplayName("validate getRentalPriceDescription")
	void testGetRentalPriceDescription() {
		String expected = "Free for less than 10 minute(s). After that, the rental fee is calculated incrementally. The starting price for the first 30 minutes is 10.000 ₫. Every next 15 minutes, pay an additional 3.000 ₫.";
		String description = calculateRentalFee.getRentalPriceDescription();
		assertEquals(expected, description);
	}

	@ParameterizedTest
	@CsvSource({
		"480, 400000",
		"600, 400000",
		"601, 390000",
		"1500, 390000",
		"1800, 390000",
		"1801, 387000",
		"3610, 381000"
	})
	@DisplayName("validate getRefund")
	void testGetRefund(long duration, double expected) {
		double refund = calculateRentalFee.getRefund(duration);
		assertEquals(expected, refund);
	}

}
