package hungnd;

import controller.PaymentController;
import exception.InvalidCardException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PaymentControllerTest {

    private PaymentController controller;

    @Before
    public void setUp() {
        controller = new PaymentController();
    }

    @Test
    public void test() {
        Map<String, Object> params = new HashMap<>();
        params.put("He", 123);
        Assert.assertFalse(controller.isValidInput(params));

        params.clear();
        params.put(PaymentController.OWNER, "HungND");
        params.put(PaymentController.CVV_CODE, "123");
        params.put(PaymentController.CARD_CODE, "0000");
        params.put(PaymentController.DATE_EXPIRED, "12");
        params.put(PaymentController.DESCRIPTION, "Chuyen tien thue xe");
        params.put(PaymentController.AMOUNT, (double) 400000);
        Assert.assertTrue(controller.isValidInput(params));

        List<String> keys = getKeys();
        keys.forEach(k -> testInvalidDataTypeOrNull(k, params));
    }

    private List<String> getKeys() {
        return List.of(PaymentController.OWNER, PaymentController.CVV_CODE, PaymentController.CARD_CODE,
                PaymentController.DATE_EXPIRED, PaymentController.DESCRIPTION, PaymentController.AMOUNT);
    }

    private void testInvalidDataTypeOrNull(String key, Map<String, Object> params) {
        params.put(key, null);
        Assert.assertFalse(controller.isValidInput(params));

        params.put(key, 123);
        Assert.assertFalse(controller.isValidInput(params));
    }

    @Test
    public void parseDateTest() {
        Assert.assertEquals("1125", controller.parseDateExpired("25/11"));
        Assert.assertEquals("131", controller.parseDateExpired("31/1"));
        Assert.assertThrows(InvalidCardException.class, () -> controller.parseDateExpired("32/11"));
        Assert.assertThrows(InvalidCardException.class, () -> controller.parseDateExpired("30/2"));
        Assert.assertThrows(InvalidCardException.class, () -> controller.parseDateExpired("-5/1"));
    }

}
