package hungnd;

import model.bike.Bike;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import repository.BikeRepository;
import repository.impl.BikeRepositoryImpl;

import java.util.List;

public class BikeRepositoryTest {

    BikeRepository repository;

    @Before
    public void setUp() {
        repository = new BikeRepositoryImpl();
    }

    @Test
    public void testFindDepositAmt() {
        Assert.assertEquals(400000, repository.getDepositAmtByBikeNo(2), 0);
        Assert.assertEquals(700000, repository.getDepositAmtByBikeNo(1), 0);
        Assert.assertNotEquals(500000, repository.getDepositAmtByBikeNo(2), 0);
    }

    @Test
    public void testFindBikeByBikeNo() {
        long bikeNo = 1;
        Bike bike = repository.findBikeByNo(bikeNo);
        Assert.assertEquals(bikeNo, bike.getBikeNo());
    }

    @Test
    public void findBikesByStationNo() {
        long stationNo = 1;
        List<Bike> bikes = repository.findAllBikes(stationNo);
        Assert.assertEquals(bikes.size(), 6);
    }
}
