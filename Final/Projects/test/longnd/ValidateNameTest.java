package longnd;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ParkingController;

public class ValidateNameTest {

    private ParkingController parkingController;

    @BeforeEach
    void setUp() throws Exception {
    	parkingController = new ParkingController();
    }

    @ParameterizedTest
    @CsvSource({
            "Bai Cat Song Hong,true",
            "B123,false",
            "%BAI DAT TRONG,false"
    })
    void test(String name, boolean expected) {
        boolean isValid = parkingController.validateName(name);
        Assertions.assertEquals(isValid, expected);
    }
}
