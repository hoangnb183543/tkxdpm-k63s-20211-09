package longnd;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ParkingController;

class ValidateDockingStationNo {
	private ParkingController parkingController;

	@BeforeEach
	void setUp() throws Exception {
		parkingController = new ParkingController();
	}
	
	@ParameterizedTest
	@CsvSource({
			"10000,true",
			"-13,false",
			"124512121212121,false",
	})
	
	void test(long no, boolean expected) {
		boolean isValid = parkingController.validateNo(no);
		assertEquals(isValid, expected);
	}

}
