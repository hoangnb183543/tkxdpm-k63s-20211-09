package mvc.tutorial;

import javax.swing.JFrame;

public class Model extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int calculationValue;
	
	public void addTwoNumbers(int firstNumber, int secondNumber) {
		calculationValue = firstNumber + secondNumber;
	}
	
	public int getCalculationValue() {
		return calculationValue;
	}
}
