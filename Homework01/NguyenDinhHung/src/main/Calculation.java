package main;

import java.util.Scanner;

public class Calculation {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the first number: ");
		double a = sc.nextDouble();
		System.out.println("Enter the second number: ");
		double b = sc.nextDouble();

		System.out.println("a + b = " + (a + b));
		System.out.println("a - b = " + (a - b));
		System.out.println("a * b = " + (a * b));
		if (b == 0) {
			System.out.println("Dividing by 0");
		} else {
			System.out.println("a / b = " + (a / b));
		}
	}
}
