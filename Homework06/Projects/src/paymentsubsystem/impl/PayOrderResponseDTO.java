package paymentsubsystem.impl;

import lombok.Builder;
import lombok.Data;
import lombok.Getter;

@Data
public class PayOrderResponseDTO {

    private String errorCode;
    private Transaction transaction;

    @Builder
    @Getter
    public static class Transaction {
        private String cardCode;
        private String owner;
        private String cvvCode;
        private String dateExpired;
        private String command;
        private String transactionContent;
        private double amount;
        private String createdAt;
        private String transactionId;
    }

}
