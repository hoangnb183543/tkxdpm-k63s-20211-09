package paymentsubsystem.impl;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author HungND
 */
public class APICallerImpl implements APICaller {

    private static final String PATCH_METHOD = "PATCH";

    @Override
    public String patch(String url, String token, String requestBody) {
        allowMethods(PATCH_METHOD);
        try {
            HttpURLConnection conn = getHttpURLConnection(url, PATCH_METHOD, token);
            Writer writer = new BufferedWriter(new OutputStreamWriter(conn.getOutputStream()));
            writer.write(requestBody);
            writer.close();
            return readResponse(conn);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null; 
    }

    private HttpURLConnection getHttpURLConnection(String url, String method, String token) throws IOException {
        URL lineApiUrl = new URL(url);
        HttpURLConnection conn = (HttpURLConnection) lineApiUrl.openConnection();
        conn.setDoInput(true);
        conn.setDoOutput(true);
        conn.setRequestMethod(method);
        conn.setRequestProperty("Content-Type", "application/json");
        conn.setRequestProperty("Authorization", "Bearer " + token);
        return conn;
    }

    private String readResponse(HttpURLConnection conn) throws IOException {
        try (BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream(), StandardCharsets.UTF_8))) {
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }
            System.out.println(response);
            return response.toString();
        }
    }

    private void allowMethods(String... methods) {
        try {
            Field methodsField = HttpURLConnection.class.getDeclaredField("methods");
            methodsField.setAccessible(true);

            Field modifiersField = Field.class.getDeclaredField("modifiers");
            modifiersField.setAccessible(true);
            modifiersField.setInt(methodsField, methodsField.getModifiers() & ~Modifier.FINAL);

            String[] oldMethods = (String[]) methodsField.get(null);
            Set<String> methodsSet = new LinkedHashSet<>(Arrays.asList(oldMethods));
            methodsSet.addAll(Arrays.asList(methods));
            String[] newMethods = methodsSet.toArray(new String[0]);

            methodsField.set(null/* static field */, newMethods);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new IllegalStateException(e);
        }
    }
}
