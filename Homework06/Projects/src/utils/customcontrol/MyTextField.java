package utils.customcontrol;

import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;

public class MyTextField extends TextField {
	private int maxLength = 255;
	private boolean required = false;
	
	public MyTextField() {
		super();
		textProperty().addListener((observable, oldValue, newValue) -> {
			setTooltip(null);
			if(required && (newValue == null || newValue.isBlank())) {
		    	setTooltip(new Tooltip("This field is required."));
		    }
		    if (newValue.length() > maxLength) {
		        setText(newValue.substring(0, maxLength));
		        setTooltip(new Tooltip("The maximum number of characters is " + maxLength));
		    }
		});
	}


	public int getMaxLength() {
		return maxLength;
	}

	public void setMaxLength(int maxLength) {
		this.maxLength = maxLength;
	}

	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}
	
}
