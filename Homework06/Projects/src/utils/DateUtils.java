package utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtils {

    public static final String DD_MM_YYYY = "dd-MM-yyyy";

    public static String convertDateToString(LocalDate date) {
        return date.format(DateTimeFormatter.ofPattern(DD_MM_YYYY));
    }

    public static String convertDateToString(LocalDate date, String pattern) {
        return date.format(DateTimeFormatter.ofPattern(pattern));
    }

}
