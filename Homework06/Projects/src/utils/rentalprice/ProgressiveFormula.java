package utils.rentalprice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Lớp Công thức tính tiền lũy tiến
 * 
 * @author PTHIEU 06.01.2022
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProgressiveFormula extends FormulaCalculatingRental {
	private int freeMinutesDuration;
	private double first30MinutesPrice;
	private double every15MinutesNextPrice;
}
