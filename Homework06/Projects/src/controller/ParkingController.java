package controller;

import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import repository.DockingStationRepository;
import repository.impl.DockingStationRepositoryImpl;

import java.util.List;

@Log
public class ParkingController extends BaseController {

    private DockingStationRepository dockingStationRepository;

    public ParkingController() {
        this.dockingStationRepository = new DockingStationRepositoryImpl();
    }

    public ParkingController(DockingStationRepository dockingStationRepository) {
        this.dockingStationRepository = dockingStationRepository;
    }

    public List<DockingStation> getAllDockingStations() {
        log.info("Start find all bike types");
        return dockingStationRepository.getAllDockingStations();
    }
    
    public int addDockingStation(DockingStation dockingStation) {
        log.info(String.format("Start add bike type: %s", dockingStation.toString()));
        if(!validate(dockingStation)) return -1; 
        int count = dockingStationRepository.addDockingStation(dockingStation);
        if (count < 1) {
            log.info("Add bike type fail.");
        }
        return count;
    }
    
    public boolean validate(DockingStation dockingStation) {
    	long no = dockingStation.getDockingStationNo();
    	String name = dockingStation.getName();
    	String address = dockingStation.getAddress();
    	long capacity = dockingStation.getCapacity();
    	if(validateNo(no) && validateName(name) 
    			&& validateAddress(address) && validateCapacity(capacity)) return true;
    	return false;
    }
    
    public boolean validateNo(long no) {
    	if (no != (long)no) return false;
    	if (no <0 || no>10000) return false;
    	return true;
    }
    
    public boolean validateName(String name) {
    	if (name == null)
			return false;
		if (name.matches("^[ A-Za-z]+$"))
			return true;
		return false;
    }
    
    public boolean validateAddress(String address) {
    	if (address == null || address=="")
			return false;
		return true;
    }
    
    public boolean validateCapacity(long capacity) {
    	if (capacity != (long)capacity) return false;
    	if (capacity <0 || capacity>10000) return false;
    	return true;
    }
}

