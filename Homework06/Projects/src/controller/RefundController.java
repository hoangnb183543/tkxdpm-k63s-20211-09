package controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import exception.InvalidCardException;
import lombok.extern.java.Log;
import model.payment.PaymentCard;
import model.payment.PaymentCode;
import model.payment.PaymentTransaction;
import paymentsubsystem.PaymentSubSystem;
import paymentsubsystem.impl.PaymentSubSystemImpl;
import repository.PaymentTransactionRepository;
import repository.impl.PaymentTransactionRepositoryImpl;

@Log
public class RefundController {

    PaymentSubSystem paymentSubSystem;
    Map<String, Object> params;
    PaymentTransactionRepository transactionRepository;
    BikeInfoController bikeInfoController;
    RentalHistoryController rentalHistoryController;

    public static final String CARD_CODE = "CARD_CODE";
    public static final String ISSUE_BANK = "ISSUE_BANK";
   
    public RefundController() {
        this.paymentSubSystem = new PaymentSubSystemImpl();
        this.transactionRepository = new PaymentTransactionRepositoryImpl();
        this.bikeInfoController = new BikeInfoController();
        this.rentalHistoryController = new RentalHistoryController();
    }

    public RefundController(Map<String, Object> params) {
        this();
        this.params = params;
    }
    
    private boolean isNullOrEmpty(String string) {
        return (string == null) || string.length() == 0 || string.trim().length() == 0;
    }
    
    public boolean isValidInput(Map<String, Object> params) {
        try {
            String cardCode = (String) params.get(CARD_CODE);
            String issueBank = (String) params.get(ISSUE_BANK);
            return !(isNullOrEmpty(cardCode) || isNullOrEmpty(issueBank));
        } catch (Exception e) {
            return false;
        }
    }
}
