package controller;

import java.util.List;

import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import repository.DockingStationRepository;
import repository.impl.DockingStationRepositoryImpl;

@Log
public class DockingStationSearchController extends BaseController {
	
	private DockingStationRepository dockingStationRepository;
	
	public DockingStationSearchController() {
		this.dockingStationRepository = new DockingStationRepositoryImpl();
	}
	
	public DockingStationSearchController(DockingStationRepository dockingStationRepository) {
		this.dockingStationRepository = dockingStationRepository;
	}
	
	public List<DockingStation> findAllDockingStations() {
		log.info("Start find all docking stations");
		return dockingStationRepository.findAllDockingStations();
	}
}
