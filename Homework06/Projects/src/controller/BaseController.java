package controller;

public class BaseController {

    private static final long DEMO_USER_NO = 1;

    protected long getCurrentUserNo() {
        return DEMO_USER_NO;
    }

}
