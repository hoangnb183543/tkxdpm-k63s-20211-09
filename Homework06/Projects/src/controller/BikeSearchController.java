package controller;

import model.bike.Bike;
import repository.BikeRepository;
import repository.impl.BikeRepositoryImpl;

import java.util.List;

import lombok.extern.java.Log;

@Log
public class BikeSearchController {

    private BikeRepository bikeRepository;

    public BikeSearchController() {
        this.bikeRepository = new BikeRepositoryImpl();
    }

    public BikeSearchController(BikeRepository bikeRepository) {
        this.bikeRepository = bikeRepository;
    }

    public List<Bike> findAllBikes() {
        log.info("Start find all bikes");
        return bikeRepository.findAllBikes();
    }

    public List<Bike> findAllBikes(long stationNo) {
        log.info(String.format("Start find all bikes in station no = %s", stationNo));
        return bikeRepository.findAllBikes(stationNo);
    }

    public Bike getByBikeNo(long bikeNo) {
        log.info(String.format("Start find bike no %d", bikeNo));
        Bike bike = bikeRepository.findBikeByNo(bikeNo);
        if (bike == null) {
            log.info("Bike not found...");
        }
        return bike;
    }

}
