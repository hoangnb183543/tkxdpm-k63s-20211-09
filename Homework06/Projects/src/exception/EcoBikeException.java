package exception;

public class EcoBikeException extends RuntimeException {

    public EcoBikeException(String message) {
        super(message);
    }

}
