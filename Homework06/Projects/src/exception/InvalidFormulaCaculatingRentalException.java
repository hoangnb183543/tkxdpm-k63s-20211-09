package exception;

/**
 * Exception liên quan đến công thức tính tiền thuê xe
 * 
 * @author PTHIEU 30.12.2021
 */
public class InvalidFormulaCaculatingRentalException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public InvalidFormulaCaculatingRentalException(String message) {
		super(message);
		
	}
}
