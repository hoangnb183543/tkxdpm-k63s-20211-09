package views.screen.searchdockingstation;

import java.io.IOException;

import javafx.stage.Stage;
import model.dockingstation.DockingStation;

public class DockingStationSearchByNameFormHandler extends DockingStationSearchFormHandler {

	public DockingStationSearchByNameFormHandler(Stage stage, String screenpath) throws IOException {
		super(stage, screenpath, "Name");
	}

	@Override
	protected boolean isContain(DockingStation d, String search) {
		return d.getName().toLowerCase().contains(search);
	}

}
