package views.screen.searchdockingstation;

import java.io.IOException;

import javafx.stage.Stage;
import model.dockingstation.DockingStation;

public class DockingStationSearchByAddressFormHandler extends DockingStationSearchFormHandler {

	public DockingStationSearchByAddressFormHandler(Stage stage, String screenpath) throws IOException {
		super(stage, screenpath, "Address");
	}

	@Override
	protected boolean isContain(DockingStation d, String search) {
		return d.getAddress().toLowerCase().contains(search);
	}

}
