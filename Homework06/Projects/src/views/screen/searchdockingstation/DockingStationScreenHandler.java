package views.screen.searchdockingstation;

import java.io.IOException;

import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.home.HomeScreenHandler;

@Log
public class DockingStationScreenHandler extends BaseScreenHandler {
	
	HomeScreenHandler homeScreenHandler;
	
	public DockingStationScreenHandler(Stage stage, String screenpath, HomeScreenHandler homeScreenHandler) throws IOException {
		super(stage, screenpath, true);
		this.homeScreenHandler = homeScreenHandler;
	}
	
	@FXML
	private void searchDockingStationByAddressBtnClicked(MouseEvent event) throws IOException {
		DockingStationSearchFormHandler handler = new DockingStationSearchByAddressFormHandler(this.stage,  Configs.SEARCH_DOCKING_STATION_FORM_PATH);
		log.info("Search docking station by address button clicked");
		handler.handleBtnClicked(handler, this.homeScreenHandler, this.homeScreenHandler, "Search docking station by address screen");
	}
	
	@FXML
	private void searchDockingStationByNameBtnClicked(MouseEvent event) throws IOException {
		DockingStationSearchFormHandler handler = new DockingStationSearchByNameFormHandler(this.stage,  Configs.SEARCH_DOCKING_STATION_FORM_PATH);
		log.info("Search docking station by name button clicked");
		handler.handleBtnClicked(handler, this.homeScreenHandler, this.homeScreenHandler, "Search docking station by name screen");
	}
	
}
