package views.screen.managebiketype;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.google.gson.Gson;

import controller.BikeTypeController;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.biketype.BikeType;
import utils.rentalprice.CalculateRentalFee;
import utils.rentalprice.ProgressiveFormula;
import utils.rentalprice.RentalPriceConstant;
import views.screen.BaseScreenHandler;

@Log
public abstract class BikeTypeDetailHandler extends BaseScreenHandler {
	@FXML
    protected ImageView ecobikeImg;
	@FXML
	protected Label titleLabel;
    @FXML
    protected TextField bikeTypeCodeTxtField;
    @FXML
    protected TextField bikeTypeNameTxtField;
    @FXML
    protected TextArea descriptionTxtField;
    @FXML
    protected TextField depositAmountTxtField;
    @FXML
    protected TextField freeMinutesDurationTxtField;
    @FXML
    protected TextField first30MinutesPriceTxtField;
    @FXML
    protected TextField every15MinutesNextPriceTxtField;
    @FXML
    protected Button saveBtn;
    @FXML
    protected Button cancelBtn;

    protected BikeTypeController controller;

    protected BikeType bikeType;
    
    private final String INVALID_DATA_MESSAGE = "Invalid data. Please check the value and try again.";

    public BikeTypeDetailHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath, true);
        this.controller = new BikeTypeController();
    }
    
    public BikeTypeDetailHandler(Stage stage, String screenPath, BikeType bikeType) throws IOException {
        this(stage, screenPath);
        this.bikeType = bikeType;
        
        bikeTypeCodeTxtField.setText(bikeType.getBikeTypeCode());
        bikeTypeNameTxtField.setText(bikeType.getBikeTypeName());
        descriptionTxtField.setText(bikeType.getBikeTypeDescription());
        depositAmountTxtField.setText(String.valueOf((int)bikeType.getDepositAmount()));
        setFormulaTxtField();
    } 
    
    @Override
    public void setScreenTitle(String string) {
    	super.setScreenTitle(string);
    	titleLabel.setText(string.toUpperCase());
    }
    
    @FXML
    protected void cancelBtnClicked(MouseEvent event) throws IOException {	
    	log.info("Cancel button clicked");
    	Map<String, Object> messages = new HashMap<>();
        getPreviousScreen().forward(messages);
        getPreviousScreen().show();
    }
    
    @FXML
    protected void saveBtnClicked(MouseEvent event) throws IOException {
    	Map<String, Object> data = new HashMap<>();
    	data.put(BikeTypeController.BIKE_TYPE_CODE, bikeTypeCodeTxtField.getText());
    	data.put(BikeTypeController.BIKE_TYPE_NAME, bikeTypeNameTxtField.getText());
    	data.put(BikeTypeController.BIKE_TYPE_DESCRIPTION, descriptionTxtField.getText());
    	data.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, depositAmountTxtField.getText());
    	// ô thiết lập giá tiền
    	data.put(RentalPriceConstant.FREE_MINUTES_DURATION, freeMinutesDurationTxtField.getText());
    	data.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, first30MinutesPriceTxtField.getText());
    	data.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, every15MinutesNextPriceTxtField.getText());
    	
    	if(controller.validateInfo(data)) {
    		saveAction();
    	} else {
    		Alert alert = new Alert(AlertType.ERROR, INVALID_DATA_MESSAGE);
    		alert.showAndWait();
    	}
    	
    }
    
    /**
     * Phương thức xử lý hành động lưu
     */
    protected abstract void saveAction();
    
    /**
     * Phương thức kiểm tra sự thay đổi dữ liệu
     * @return true - có thay đổi, false - không có thay đổi
     */
	protected boolean hasChanged() {
		String bikeTypeCode = bikeTypeCodeTxtField.getText();
		String bikeTypeName = bikeTypeNameTxtField.getText();
		String description = descriptionTxtField.getText();
		double depositAmount = Double.parseDouble(depositAmountTxtField.getText());
		
		if(bikeTypeCode.equals(this.bikeType.getBikeTypeCode())
				&& bikeTypeName.equals(this.bikeType.getBikeTypeName())
				&& description.equals(this.bikeType.getBikeTypeDescription())
				&& (int)depositAmount == (int)this.bikeType.getDepositAmount()
				&& hasChangedFormula() == false) {
			return false;
		}
		
		return true;
	}
	
    
    /**
     * Phương thức lấy ra chuỗi json công thức tính giá thuê xe
     * @return chuỗi json
     */
    public String getFormula() {
    	int freeMinutesDuration = Integer.parseInt(freeMinutesDurationTxtField.getText());
		double first30MinutesPrice = Double.parseDouble(first30MinutesPriceTxtField.getText());
		double every15MinutesNextPrice = Double.parseDouble(every15MinutesNextPriceTxtField.getText());
		ProgressiveFormula progressiveFormula = new ProgressiveFormula(freeMinutesDuration, first30MinutesPrice, every15MinutesNextPrice);
		String formula = new Gson().toJson(progressiveFormula);
		return formula;
    }
    
    /**
     * Phương thức set giá trị cho các ô thiết lập giá thuê xe
     * @return chuỗi json
     */
    public void setFormulaTxtField() {
    	try {
    		String formula = this.bikeType.getFormula();
    		if(formula != null) {
    			ProgressiveFormula progressiveFormula = new Gson().fromJson(formula, ProgressiveFormula.class);
            	
            	freeMinutesDurationTxtField.setText(String.valueOf(progressiveFormula.getFreeMinutesDuration()));
            	first30MinutesPriceTxtField.setText(String.valueOf((int)progressiveFormula.getFirst30MinutesPrice()));
            	every15MinutesNextPriceTxtField.setText(String.valueOf((int)progressiveFormula.getEvery15MinutesNextPrice()));
    		}
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
    
    /**
     * Phương thức kiểm tra sự thay đổi ở các ô thiết lập giá thuê xe
     * @return
     */
    public boolean hasChangedFormula() {
    	int freeMinutesDuration = Integer.parseInt(freeMinutesDurationTxtField.getText());
		double first30MinutesPrice = Double.parseDouble(first30MinutesPriceTxtField.getText());
		double every15MinutesNextPrice = Double.parseDouble(every15MinutesNextPriceTxtField.getText());
		
		try {
    		String formula = this.bikeType.getFormula();
    		if(formula != null) {
    			ProgressiveFormula progressiveFormula = new Gson().fromJson(formula, ProgressiveFormula.class);
            	
    			if(freeMinutesDuration == progressiveFormula.getFreeMinutesDuration()
    	    			&& (int)first30MinutesPrice == progressiveFormula.getFirst30MinutesPrice()
    	    			&& (int)every15MinutesNextPrice == progressiveFormula.getEvery15MinutesNextPrice()) {
    	    		return false;
    	    	}
    		}
    		
		} catch (Exception e) {
			e.printStackTrace();
		}
   	
		return true;
    }
}
