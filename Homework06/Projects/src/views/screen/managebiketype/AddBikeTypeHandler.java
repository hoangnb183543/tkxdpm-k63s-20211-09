package views.screen.managebiketype;

import java.io.IOException;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.biketype.BikeType;

@Log
public class AddBikeTypeHandler extends BikeTypeDetailHandler {
	private final String FAILURE_MESSAGE = "Failed to add new bike type.";
	private final String SUCCESS_MESSAGE = "Add new bike type successfully.";
	private final String SCREEN_TITLE = "Add new bike type";
	
    public AddBikeTypeHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath);
        setScreenTitle(SCREEN_TITLE);
    }
    

	@Override
	protected void saveAction() {
		String bikeTypeCode = bikeTypeCodeTxtField.getText();
		String bikeTypeName = bikeTypeNameTxtField.getText();
		String description = descriptionTxtField.getText();
		double depositAmount = Double.parseDouble(depositAmountTxtField.getText());		
		String formula = getFormula();
		
		BikeType bikeType = BikeType.builder()
				.bikeTypeCode(bikeTypeCode)
				.bikeTypeName(bikeTypeName)
				.bikeTypeDescription(description)
				.depositAmount(depositAmount)
				.formula(formula)
				.build();
		
		int count = controller.addBikeType(bikeType);
		if(count < 1) {
			log.info(FAILURE_MESSAGE);
			Alert alert = new Alert(AlertType.ERROR, FAILURE_MESSAGE);
			alert.showAndWait();
		} else {
			log.info(SUCCESS_MESSAGE);
			Alert alert = new Alert(AlertType.INFORMATION, SUCCESS_MESSAGE);
			alert.showAndWait();
			getPreviousScreen().forward(null);
			getPreviousScreen().show();
		}
		
	}
}
