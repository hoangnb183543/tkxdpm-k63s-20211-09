package views.screen.home;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.bikesearch.BikeSearchScreenHandler;
import views.screen.managebiketype.ManageBikeTypeHandler;
import views.screen.parking.ParkingScreenHandler;
import views.screen.returnbike.NotFoundBikeHandler;
import views.screen.returnbike.ReturnBikeHandler;
import views.screen.searchdockingstation.DockingStationScreenHandler;

import java.io.IOException;

import controller.ReturnBikeController;

@Log
public class HomeScreenHandler extends BaseScreenHandler {

    @FXML
    private Button rentBikeBtn;
    
    @FXML
    private Button manageBikeTypeBtn;
    
    @FXML
    private Button returnBikeBtn;

    @FXML
    private ImageView ecobikeImg;

    public HomeScreenHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath, true);
    }

    @FXML
    private void rentBikeBtnClicked(MouseEvent event) throws IOException {
        BikeSearchScreenHandler handler = new BikeSearchScreenHandler(this.stage, Configs.SEARCH_BIKE_SCREEN_PATH);
        log.info("Rent bike button clicked");
        handler.handleBtnClicked(handler, this, this, "Search bike screen");;
    }

    
    @FXML
    private void manageBikeTypeBtnClicked(MouseEvent event) throws IOException {
        ManageBikeTypeHandler handler = new ManageBikeTypeHandler(this.stage, Configs.MANAGE_BIKE_TYPE_SCREEN_PATH);
        log.info("Manage bike button clicked");
        handler.handleBtnClicked(handler, this, this, "Bike type management");;
    }

    @FXML
    private void parkingBtnClicked(MouseEvent event) throws IOException {
        ParkingScreenHandler handler = new ParkingScreenHandler(this.stage, Configs.PARKING_MANAGER_PATH);
        log.info("Manage docking stataion button clicked");
        handler.handleBtnClicked(handler, this, this, "Manage docking station screen");;
    }
    
    @FXML
    private void searchDockingStationBtnClicked(MouseEvent event) throws IOException {
    	DockingStationScreenHandler handler = new DockingStationScreenHandler(this.stage, Configs.SEARCH_DOCKING_STATION_SCREEN_PATH, this);
        log.info("Search docking station button clicked");
    	handler.handleBtnClicked(handler, this, this, "Search docking station screen");
    }
    
    @FXML
    private void returnBikeBtnClicked(MouseEvent event) throws IOException {
    	log.info("Return bike button clicked");
    	ReturnBikeController controller = new ReturnBikeController();
		if (!controller.isRenting()) {
			NotFoundBikeHandler handler = new NotFoundBikeHandler(this.stage, Configs.NOT_FOUND_BIKE_PATH);
	        handler.handleBtnClicked(handler, this, this, "Return bike");
		} else {
	        ReturnBikeHandler handler = new ReturnBikeHandler(this.stage, Configs.CHOOSE_BIKE_TO_RETURN);
	        
	        handler.handleBtnClicked(handler, this, this, "Return bike");
		}
    }
}
