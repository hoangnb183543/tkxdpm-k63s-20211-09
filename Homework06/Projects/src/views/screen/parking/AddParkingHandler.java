package views.screen.parking;

import java.io.IOException;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.dockingstation.DockingStation;
import utils.Configs;
import views.screen.BaseScreenHandler;
import views.screen.popup.PopupScreenHandler;
import controller.ParkingController;

@Log
public class AddParkingHandler extends BaseScreenHandler {
	@FXML
    private ImageView ecobikeImg;
    @FXML
    private TextField parkingCodeTxtField;
    @FXML
    private TextField parkingNameTxtField;
    @FXML
    private TextField addressTxtField;
    @FXML
    private TextField capacityTxtField;
	
	ParkingController controller;
	public AddParkingHandler(Stage stage, String screenPath) throws IOException {
        super(stage, screenPath, true);
        this.controller = new ParkingController();
    }
	
	@FXML
    private void addBtnClicked(MouseEvent event) throws IOException {		
		log.info("Add docking station button clicked");
		
		int dockingStationCode = Integer.parseInt(parkingCodeTxtField.getText());
		String name = parkingNameTxtField.getText();
		String address = addressTxtField.getText();
		int capacity = Integer.parseInt(capacityTxtField.getText());
		
		DockingStation dockingStation = DockingStation.builder()
				.dockingStationNo(dockingStationCode)
				.name(name)
				.address(address)
				.capacity(capacity).build();
		
		int count = controller.addDockingStation(dockingStation);
		if(count == 0) {
			PopupScreenHandler.error("Error when add new station. Please try again");
		} else if(count ==-1){
			PopupScreenHandler.error("Data has error");
		} else{
			showResult();
		}	
    }
	
	@FXML
    private void showResult() throws IOException {		
		log.info("Add bike type button clicked");
		ResultAddHandler resultHandler;
		resultHandler = new ResultAddHandler(stage, Configs.RESULT_ADD_PARKING_SCREEN_PATH);
		resultHandler.setHomeScreenHandler(homeScreenHandler);
		resultHandler.setScreenTitle("Result");
		resultHandler.setPreviousScreen(this);
		resultHandler.show();	   
    }
}
