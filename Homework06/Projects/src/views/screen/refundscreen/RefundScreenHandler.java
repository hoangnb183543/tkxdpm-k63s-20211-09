package views.screen.refundscreen;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import controller.RefundController;
import controller.RentalHistoryController;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import repository.RentalHistoryRepository;
import utils.Configs;
import utils.CurrencyUtils;
import views.screen.BaseScreenHandler;
import views.screen.bikesearch.BikeInfoScreenHandler;

@Log
public class RefundScreenHandler  extends BaseScreenHandler {
	@FXML
    private TextField cardNumber;
    @FXML
    private TextField issueBank;
    @FXML
    private Label price;
    @FXML
    private Button cancelBtn;
    @FXML
    private Button confirmBtn;
    
    private double amount;
    private long bikeNo;
    
	public RefundScreenHandler(Stage stage, String screenPath, double amount, long bikeNo) throws IOException {
		super(stage, screenPath, true);
		this.amount = amount;
		this.bikeNo = bikeNo;

		price.setText(CurrencyUtils.convertToString((double) this.amount));
		cancelBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                getPreviousScreen().show();
            }
        });
		
		cardNumber.textProperty().addListener((obs, oldText, newText) -> {
			this.confirmBtn.setDisable(newText.isEmpty());
	    });
		
		issueBank.textProperty().addListener((obs, oldText, newText) -> {
			this.confirmBtn.setDisable(newText.isEmpty());
	    });
	}
	
    private Map<String, Object> getUserInputs() {
        Map<String, Object> params = new HashMap<>();
        params.put(RefundController.ISSUE_BANK, issueBank.getText());
        params.put(RefundController.CARD_CODE, cardNumber.getText());
        return params;
    }

    @FXML
    private void confirmRefundBtnClicked(MouseEvent event) throws IOException {
    	log.info("Confirm refund button clicked");
    	RentalHistoryController historyController = new RentalHistoryController();
 
    	historyController.saveReturnHistory(this.bikeNo);
    	
    	RefundResultHandler handler = new RefundResultHandler(this.stage, Configs.REFUND_RESULT_PATH, "You were haved return " + CurrencyUtils.convertToString(amount));
        
        handler.setHomeScreenHandler(homeScreenHandler);
        handler.setScreenTitle("Refund Result");
        handler.setPreviousScreen(prev);
        handler.show();
    }
}
