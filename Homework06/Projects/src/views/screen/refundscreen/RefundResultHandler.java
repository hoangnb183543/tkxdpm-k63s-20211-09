package views.screen.refundscreen;

import java.io.IOException;
import java.util.Map;

import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import views.screen.BaseScreenHandler;

public class RefundResultHandler extends BaseScreenHandler {

	@FXML
    private Label paymentMessage;
    @FXML
    private Button homeBtn;
    
    public RefundResultHandler(Stage stage, String screenPath, String message) throws IOException {
		super(stage, screenPath, true);
		paymentMessage.setText(message);

		homeBtn.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                homeScreenHandler.show();
            }
        });
	}

}
