package views.screen.popup;

import javafx.animation.PauseTransition;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import utils.Configs;
import views.screen.BaseScreenHandler;

import java.io.IOException;

public class PopupScreenHandler extends BaseScreenHandler {

    @FXML
    ImageView tickicon;

    @FXML
    Label message;


    public PopupScreenHandler(Stage stage) throws IOException {
        super(stage, Configs.POPUP_PATH);
    }

    private static PopupScreenHandler popup(String message, String imagePath, Boolean undecorated) throws IOException {
        PopupScreenHandler popup = new PopupScreenHandler(new Stage());
        if (undecorated) {
            popup.stage.initStyle(StageStyle.UNDECORATED);
        }
        popup.message.setText(message);
        popup.setImage(imagePath);
        return popup;
    }

    public static void success(String message) throws IOException {
        popup(message, Configs.IMAGE_PATH + "/" + "tickgreen.png", true).show(true);
    }

    public static void error(String message) throws IOException {
        popup(message, Configs.IMAGE_PATH + "/" + "tickerror.png", false).show(false);
    }

    public static PopupScreenHandler loading(String message) throws IOException {
        return popup(message, Configs.IMAGE_PATH + "/" + "loading.gif", true);
    }

    public void setImage(String path) {
        super.setImage(tickicon, path);
    }

    public void show(Boolean autoClose) {
        super.show();
        if (autoClose) {
            close(0.8);
        }
    }

    public void show(double time) {
        super.show();
        close(time);
    }

    public void close(double time) {
        PauseTransition delay = new PauseTransition(Duration.seconds(time));
        delay.setOnFinished( event -> stage.close() );
        delay.play();
    }

}
