package views.screen.bikesearch;

import controller.BikeSearchController;
import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import lombok.extern.java.Log;
import model.bike.Bike;
import utils.Configs;
import utils.DateUtils;
import views.screen.BaseScreenHandler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Log
public class BikeSearchScreenHandler extends BaseScreenHandler {

    @FXML
    private ImageView ecobikeImg;
    @FXML
    private TextField keywordTxtField;
    @FXML
    private TableView<Bike> tableView;
    @FXML
    private TableColumn<Bike, String> codeTableColumn;
    @FXML
    private TableColumn<Bike, String> nameTableColumn;
    @FXML
    private TableColumn<Bike, String> typeTableColumn;
    @FXML
    private TableColumn<Bike, Double> weightTableColumn;
    @FXML
    private TableColumn<Bike, String> licenseTableColumn;
    @FXML
    private TableColumn<Bike, String> manuDateTableColumn;
    @FXML
    private TableColumn<Bike, String> producerTableColumn;
    @FXML
    private TableColumn<Bike, Integer> costTableColumn;

    ObservableList<Bike> bikeObservableList;

    BikeSearchController controller;

    Map<String, Object> messages;

    public static final String SCREEN_TITLE = "Bike search screen";
    private final String NEXT_SCREEN_TITLE = "Bike information";
    public static final String STATION_NO = "STATION_NO";

    public BikeSearchScreenHandler(Stage stage, String screenPath) throws IOException {
        this(stage, screenPath, null);
    }

    public BikeSearchScreenHandler(Stage stage, String screenPath, Map<String, Object> messages)
            throws IOException {
        super(stage, screenPath, true);
        this.controller = new BikeSearchController();
        this.messages = messages;
        setTableViewData();
        setTableViewClicked(stage, this);
    }

    private void setTableViewClicked(Stage stage, BaseScreenHandler prev) {
        tableView.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                if (event.getClickCount() == 2) {
                    try {
                        Map<String, Object> messages = new HashMap<>();
                        long bikeNo = tableView.getSelectionModel().getSelectedItem().getBikeNo();
                        messages.put(BikeInfoScreenHandler.BIKE_NO, bikeNo);

                        BikeInfoScreenHandler bikeInfoScreenHandler = new BikeInfoScreenHandler(stage,
                                Configs.VIEW_BIKE_INFO_SCREEN_PATH,
                                messages);
                        bikeInfoScreenHandler.setHomeScreenHandler(homeScreenHandler);
                        bikeInfoScreenHandler.setScreenTitle(NEXT_SCREEN_TITLE);
                        bikeInfoScreenHandler.setPreviousScreen(prev);
                        bikeInfoScreenHandler.show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private void setTableViewData() {
        bikeObservableList = FXCollections.observableArrayList();
        long stationNo = -1;
        try {
            stationNo = (Long) this.messages.get(STATION_NO);
            log.info(String.format("Select all bikes in station %d", stationNo));
        } catch (Exception e) {
            log.info("Select all bikes");
        }
        if (stationNo != -1) {
            bikeObservableList.addAll(controller.findAllBikes(stationNo));
        } else {
            bikeObservableList.addAll(controller.findAllBikes());
        }
        codeTableColumn.setCellValueFactory(new PropertyValueFactory<>("bikeCode"));
        nameTableColumn.setCellValueFactory(new PropertyValueFactory<>("name"));
        typeTableColumn.setCellValueFactory(new PropertyValueFactory<>("type"));
        weightTableColumn.setCellValueFactory(new PropertyValueFactory<>("weight"));
        licenseTableColumn.setCellValueFactory(new PropertyValueFactory<>("licensePlate"));
        manuDateTableColumn.setCellValueFactory(bike -> new ReadOnlyStringWrapper(
                DateUtils.convertDateToString(bike.getValue().getManufacturingDate())));
        producerTableColumn.setCellValueFactory(new PropertyValueFactory<>("producer"));
        costTableColumn.setCellValueFactory(new PropertyValueFactory<>("cost"));
        tableView.setItems(bikeObservableList);

        FilteredList<Bike> filteredList = new FilteredList<>(bikeObservableList, b -> true);
        keywordTxtField.textProperty().addListener(((observableValue, oldValue, newValue) -> {
            filteredList.setPredicate(bike -> {
                if (newValue.isEmpty() || newValue.isBlank()) {
                    return true;
                }

                String search = newValue.toLowerCase();
                return isContain(bike.getBikeCode(), search) || isContain(bike.getName(), search);
            });
        }));

        SortedList<Bike> sortedList = new SortedList<>(filteredList);
        sortedList.comparatorProperty().bind(tableView.comparatorProperty());
        tableView.setItems(sortedList);
    }

    private boolean isContain(String field, String search) {
        return field.toLowerCase().contains(search);
    }
}
