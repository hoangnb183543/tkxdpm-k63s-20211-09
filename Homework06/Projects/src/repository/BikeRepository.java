package repository;

import model.bike.Bike;
import model.bike.BikeStatus;

import java.util.List;

public interface BikeRepository {

    List<Bike> findAllBikes();

    List<Bike> findAllBikes(long stationNo);

    Bike findBikeByNo(long bikeNo);

    Double getDepositAmtByBikeNo(long bikeNo);

    Bike updateBikeStatus(long bikeNo, BikeStatus newStatus);

    Bike save(Bike bike);

}
