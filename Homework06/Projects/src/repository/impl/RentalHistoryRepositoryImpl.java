package repository.impl;

import model.dbconnection.SQLLiteConnection;
import model.rentalhistory.BikeRentalHistory;
import model.rentalhistory.UserAction;
import repository.RentalHistoryRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class RentalHistoryRepositoryImpl implements RentalHistoryRepository {

    private final String SELECT = "SELECT his.RENTAL_HISTORY_NO, his.BIKE_NO, " +
            "his.USER_NO, his.CREATED_ON, his.USER_ACTION " +
            "FROM BIKE_RENTAL_HISTORY his ";

    @Override
    public List<BikeRentalHistory> findHistoriesByUserNo(long userNo) {
        List<BikeRentalHistory> histories = new ArrayList<>();
        String sql = SELECT + " WHERE his.USER_NO = ?";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setLong(1, userNo);
            ResultSet res = statement.executeQuery();
            while (res.next()) {
                histories.add(BikeRentalHistory.builder()
                                .historyNo(res.getLong("RENTAL_HISTORY_NO"))
                                .userNo(res.getLong("USER_NO"))
                                .bikeNo(res.getLong("BIKE_NO"))
                                .createdOn(res.getTimestamp("CREATED_ON").toLocalDateTime())
                                .userAction(UserAction.findByAction(res.getString("USER_ACTION")))
                                .build());
            }
            return histories;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return histories;
    }

    @Override
    public BikeRentalHistory save(BikeRentalHistory history) {
        if (!isValidNewHistory(history)) {
            return null;
        }
        String sql = "INSERT INTO BIKE_RENTAL_HISTORY (USER_NO, BIKE_NO, CREATED_ON, USER_ACTION) " +
                "VALUES (?, ?, ? , ?)";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql,
                    Statement.RETURN_GENERATED_KEYS);
            statement.setLong(1, history.getUserNo());
            statement.setLong(2, history.getBikeNo());
            statement.setTimestamp(3, Timestamp.valueOf(history.getCreatedOn()));
            statement.setString(4, history.getUserAction().getAction());
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Saving history failed...");
            }

            try (ResultSet key = statement.getGeneratedKeys()) {
                if (key.next()) {
                    history.setHistoryNo(key.getLong(1));
                } else {
                    throw new SQLException("Saving history failed, no ID obtained...");
                }
            }
            return history;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean isValidNewHistory(BikeRentalHistory history) {
        return history != null && history.getCreatedOn() != null && history.getUserAction() != null;
    }
    
    public List<BikeRentalHistory> findBikeIsRenting() {
        List<BikeRentalHistory> histories = new ArrayList<>();
        String sql = SELECT + " WHERE his.userAction = ?";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setNString(1, UserAction.RENT.getAction());
            ResultSet res = statement.executeQuery();
            while (res.next()) {
                histories.add(BikeRentalHistory.builder()
                                .historyNo(res.getLong("RENTAL_HISTORY_NO"))
                                .userNo(res.getLong("USER_NO"))
                                .bikeNo(res.getLong("BIKE_NO"))
                                .createdOn(res.getTimestamp("CREATED_ON").toLocalDateTime())
                                .userAction(UserAction.findByAction(res.getString("USER_ACTION")))
                                .build());
            }
            return histories;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return histories;
    }

}
