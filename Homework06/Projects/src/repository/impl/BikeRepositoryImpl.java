package repository.impl;

import lombok.extern.java.Log;
import model.bike.Bike;
import model.bike.BikeStatus;
import model.bike.ElectricBike;
import model.dbconnection.SQLLiteConnection;
import repository.BikeRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Log
public class BikeRepositoryImpl implements BikeRepository {

    private final String SELECT = "SELECT b.BIKE_NO, b.BIKE_NAME, bType.BIKE_TYPE_NAME, " +
            "b.BIKE_WEIGHT, b.LICENSE_PLATE, b.MANUFACTURING_DATE, " +
            "b.PRODUCER, b.BIKE_COST, b.BIKE_STATUS, b.BIKE_CODE, " +
            "b.BATTERY_PERCENTAGE, b.LOAD_CYCLES, b.USAGE_TIME_REMAINING_IN_SECOND " +
            "FROM BIKE b " +
            "LEFT JOIN BIKE_TYPE bType ON bType.BIKE_TYPE_NO = b.BIKE_TYPE_NO ";

    private final String INSERT_BIKE = "INSERT INTO BIKE (BIKE_NAME, BIKE_TYPE_NO, BIKE_WEIGHT, LICENSE_PLATE, " +
            "MANUFACTURING_DATE, PRODUCER, BIKE_COST, BIKE_STATUS, BIKE_CODE) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

    private final String INSERT_ELECTRIC_BIKE = "INSERT INTO BIKE (BIKE_NAME, BIKE_TYPE_NO, BIKE_WEIGHT, " +
            "LICENSE_PLATE, MANUFACTURING_DATE, PRODUCER, BIKE_COST, BIKE_STATUS, BIKE_CODE, " +
            "BATTERY_PERCENTAGE, LOAD_CYCLES, USAGE_TIME_REMAINING_IN_SECOND) " +
            "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

    @Override
    public List<Bike> findAllBikes() {
        List<Bike> bikes = new ArrayList<>();
        try {
            Statement statement = SQLLiteConnection.getConnection().createStatement();
            ResultSet res = statement.executeQuery(SELECT);
            while (res.next()) {
                bikes.add(getBike(res));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bikes;
    }

    @Override
    public List<Bike> findAllBikes(long stationNo) {
        List<Bike> bikes = new ArrayList<>();
        try {
            String sql = SELECT + " WHERE b.CURRENT_DOCKING_STATION_NO = ?";
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setLong(1, stationNo);
            ResultSet res = statement.executeQuery();
            while (res.next()) {
                bikes.add(getBike(res));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bikes;
    }

    private Bike getBike(ResultSet res) throws SQLException {
        String bikeType = res.getString("BIKE_TYPE_NAME");
        if (bikeType.equalsIgnoreCase("Electric")) {
            return ElectricBike.builder()
                    .bikeNo(res.getInt("BIKE_NO"))
                    .name(res.getString("BIKE_NAME"))
                    .type(res.getString("BIKE_TYPE_NAME"))
                    .weight(res.getDouble("BIKE_WEIGHT"))
                    .licensePlate(res.getString("LICENSE_PLATE"))
                    .manufacturingDate(res.getDate("MANUFACTURING_DATE") != null ?
                            res.getDate("MANUFACTURING_DATE").toLocalDate() : null)
                    .producer(res.getString("PRODUCER"))
                    .cost(res.getInt("BIKE_COST"))
                    .status(BikeStatus.findByName(res.getString("BIKE_STATUS")))
                    .bikeCode(res.getString("BIKE_CODE"))
                    .batteryPercentage(res.getInt("BATTERY_PERCENTAGE"))
                    .loadCycles(res.getInt("LOAD_CYCLES"))
                    .usageTimeRemainingInSecond(res.getInt("USAGE_TIME_REMAINING_IN_SECOND"))
                    .build();
        } else {
            return Bike.builder()
                    .bikeNo(res.getInt("BIKE_NO"))
                    .name(res.getString("BIKE_NAME"))
                    .type(res.getString("BIKE_TYPE_NAME"))
                    .weight(res.getDouble("BIKE_WEIGHT"))
                    .licensePlate(res.getString("LICENSE_PLATE"))
                    .manufacturingDate(res.getDate("MANUFACTURING_DATE") != null ?
                            res.getDate("MANUFACTURING_DATE").toLocalDate() : null)
                    .producer(res.getString("PRODUCER"))
                    .cost(res.getInt("BIKE_COST"))
                    .status(BikeStatus.findByName(res.getString("BIKE_STATUS")))
                    .bikeCode(res.getString("BIKE_CODE"))
                    .build();
        }
    }

    @Override
    public Bike findBikeByNo(long bikeNo) {
        String query = SELECT +
                "WHERE b.BIKE_NO = ? " +
                "LIMIT 1 ";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(query);
            statement.setLong(1, bikeNo);
            ResultSet res = statement.executeQuery();
            return getBike(res);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Double getDepositAmtByBikeNo(long bikeNo) {
        String sql = "SELECT bType.DEPOSIT_AMT " +
                "FROM BIKE b " +
                "LEFT JOIN BIKE_TYPE bType ON b.BIKE_TYPE_NO = bType.BIKE_TYPE_NO " +
                "WHERE b.BIKE_NO = ?";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setLong(1, bikeNo);
            ResultSet res = statement.executeQuery();
            return res.getDouble("DEPOSIT_AMT");
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bike updateBikeStatus(long bikeNo, BikeStatus newStatus) {
        String sql = "UPDATE BIKE " +
                "SET BIKE_STATUS = ? " +
                "WHERE BIKE_NO = ?";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setString(1, newStatus.getName());
            statement.setLong(2, bikeNo);
            statement.executeUpdate();
            return findBikeByNo(bikeNo);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public Bike save(Bike bike) {
        log.info("Saving bike " + bike);
        try {
            Integer bikeTypeNo = findBikeTypeNoByName(bike.getType());
            if (bikeTypeNo == null) {
                log.info("Not found bike type...");
                return null;
            }

            PreparedStatement statement;
            if (bike instanceof ElectricBike) {
                statement = getPreparedStatement(INSERT_ELECTRIC_BIKE, bikeTypeNo, bike);

                ElectricBike eBike = (ElectricBike) bike;
                statement.setInt(10, eBike.getBatteryPercentage());
                statement.setInt(11, eBike.getLoadCycles());
                statement.setInt(12, eBike.getUsageTimeRemainingInSecond());
            } else {
                statement = getPreparedStatement(INSERT_BIKE, bikeTypeNo, bike);
            }
            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0) {
                throw new SQLException("Saving bike failed, no rows affected.");
            }

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                    bike.setBikeNo(generatedKeys.getLong(1));
                }
                else {
                    throw new SQLException("Saving bike failed, no ID obtained.");
                }
            }

            return bike;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private PreparedStatement getPreparedStatement(String sql, Integer bikeTypeNo, Bike bike) throws SQLException {
        PreparedStatement statement;
        statement = SQLLiteConnection.getConnection().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
        statement.setString(1, bike.getName());
        statement.setInt(2, bikeTypeNo);
        statement.setDouble(3, bike.getWeight());
        statement.setString(4, bike.getLicensePlate());
        statement.setTimestamp(5, bike.getManufacturingDate() != null ?
                Timestamp.valueOf(bike.getManufacturingDate().atStartOfDay()) : null);
        statement.setString(6, bike.getProducer());
        statement.setInt(7, bike.getCost());
        statement.setString(8, bike.getStatus() != null ? bike.getStatus().getName() : null);
        statement.setString(9, bike.getBikeCode());
        return statement;
    }

    private Integer findBikeTypeNoByName(String bikeTypeName) {
        String sql = "SELECT bType.BIKE_TYPE_NO FROM BIKE_TYPE bType " +
                "WHERE bType.BIKE_TYPE_NAME = ? " +
                "LIMIT 1";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setString(1, bikeTypeName);
            ResultSet res = statement.executeQuery();
            return res.getInt("BIKE_TYPE_NO");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
