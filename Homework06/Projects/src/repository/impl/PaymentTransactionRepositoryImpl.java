package repository.impl;

import lombok.extern.java.Log;
import model.dbconnection.SQLLiteConnection;
import model.payment.PaymentCode;
import model.payment.PaymentTransaction;
import repository.PaymentTransactionRepository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Timestamp;

@Log
public class PaymentTransactionRepositoryImpl implements PaymentTransactionRepository {

    @Override
    public PaymentTransaction getTransactionByNo(String transNo) {
        String sql = "SELECT pt.PAYMENT_TRANSACTION_NO, pt.AMOUNT, " +
                "   pt.CONTENT, pt.CREATED_AT, pt.PAYMENT_CODE " +
                "FROM PAYMENT_TRANSACTION pt " +
                "WHERE pt.PAYMENT_TRANSACTION_NO = ?";
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setString(1, transNo);
            ResultSet res = statement.executeQuery();
            return PaymentTransaction.builder()
                    .paymentCode(PaymentCode.findByCode(res.getString("PAYMENT_CODE")))
                    .content(res.getString("CONTENT"))
                    .amount(res.getDouble("AMOUNT"))
                    .transactionNo(res.getString("PAYMENT_TRANSACTION_NO"))
                    .createdAt(res.getTimestamp("CREATED_AT").toLocalDateTime())
                    .build();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public PaymentTransaction save(PaymentTransaction transaction) {
        if (transaction.getTransactionNo() == null) {
            return null;
        }

        String sql = "INSERT INTO PAYMENT_TRANSACTION " +
                "(PAYMENT_TRANSACTION_NO, AMOUNT, CONTENT, CREATED_AT, PAYMENT_CODE) " +
                "VALUES (?, ?, ?, ?, ?)";
        log.info(String.format("Save query: %s", sql));
        try {
            PreparedStatement statement = SQLLiteConnection.getConnection().prepareStatement(sql);
            statement.setString(1, transaction.getTransactionNo());
            statement.setDouble(2, transaction.getAmount());
            statement.setString(3, transaction.getContent());
            statement.setTimestamp(4, Timestamp.valueOf(transaction.getCreatedAt()));
            statement.setString(5, transaction.getPaymentCode().getCode());
            statement.executeUpdate();
            return transaction;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
