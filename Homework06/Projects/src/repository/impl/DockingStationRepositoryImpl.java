package repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.sql.ResultSet;
import java.sql.Statement;

import model.dbconnection.SQLLiteConnection;
import model.dockingstation.DockingStation;
import repository.DockingStationRepository;

public class DockingStationRepositoryImpl implements DockingStationRepository {

	private final String SELECT = "SELECT DOCKING_STATION_NO, DOCKING_STATION_NAME, DOCKING_STATION_ADDRESS, CAPACITY FROM DOCKING_STATION";
	
	@Override
	public List<DockingStation> findAllDockingStations() {
		List<DockingStation> dockingStations = new ArrayList<>();
		try {
			Statement statement = SQLLiteConnection.getConnection().createStatement();
			ResultSet res = statement.executeQuery(SELECT);
			while (res.next()) {
				dockingStations.add(DockingStation.builder()
							.dockingStationNo(res.getLong("DOCKING_STATION_NO"))
							.name(res.getString("DOCKING_STATION_NAME"))
							.address(res.getString("DOCKING_STATION_ADDRESS"))
							.capacity(res.getLong("CAPACITY"))
							.build());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return dockingStations;
	}

	@Override
	public List<DockingStation> getAllDockingStations() {
		String query = "SELECT * FROM docking_station";
		try {
			Statement statement = SQLLiteConnection.getConnection().createStatement();
			ResultSet res = statement.executeQuery(query);
			List<DockingStation> dockerStations = new ArrayList<>();
			while (res.next()) {
				dockerStations.add(DockingStation.builder()
						.dockingStationNo(res.getLong("DOCKING_STATION_NO"))
						.name(res.getString("DOCKING_STATION_NAME"))
						.address(res.getString("DOCKING_STATION_ADDRESS"))
						.capacity(res.getLong("CAPACITY"))
						.build());
			}
			return dockerStations;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	@Override
	public int addDockingStation(DockingStation dockingStation) {
		String sql = "INSERT INTO docking_station(docking_station_no, docking_station_name, docking_station_address, capacity) VALUES(?, ?, ?, ?)";
		try {
			Connection conn = SQLLiteConnection.getConnection();
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setLong(1, dockingStation.getDockingStationNo());
			ps.setString(2, dockingStation.getName());
			ps.setString(3, dockingStation.getAddress());
			ps.setLong(4, dockingStation.getCapacity());

			int count = ps.executeUpdate();

			return count;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return 0;
	}
}
