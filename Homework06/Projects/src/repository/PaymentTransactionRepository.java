package repository;

import model.payment.PaymentTransaction;

public interface PaymentTransactionRepository {

    PaymentTransaction getTransactionByNo(String transNo);

    PaymentTransaction save(PaymentTransaction transaction);

}
