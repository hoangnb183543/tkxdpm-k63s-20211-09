package repository;

import java.util.List;

import model.dockingstation.DockingStation;

public interface DockingStationRepository {
	
	List<DockingStation> findAllDockingStations();
	List<DockingStation> getAllDockingStations();
	int addDockingStation(DockingStation dockingStation);
}
