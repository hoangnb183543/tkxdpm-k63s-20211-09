package repository;

import java.util.List;

import model.biketype.BikeType;

public interface IBikeTypeRepository {
	List<BikeType> getAllBikeTypes();
	
	BikeType getBikeTypeByNo(long bikeTypeNo);
	
	int addBikeType(BikeType bikeType);
	
	int updateBikeType(BikeType bikeType);
	
	int deleteBikeTypeByNo(long bikeTypeNo);
}
