package model.rentalhistory;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Getter
public enum UserAction {

    RENT("RENT"),
    RETURN("RETURN");

    private String action;

    public static UserAction findByAction(String action) {
        for (UserAction history : UserAction.values()) {
            if (history.getAction().equalsIgnoreCase(action)) {
                return history;
            }
        }
        return null;
    }
}
