package model.bike;

import lombok.*;

import java.security.InvalidParameterException;
import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
public class ElectricBike extends Bike {

    private int batteryPercentage;
    private int loadCycles;
    private int usageTimeRemainingInSecond;

    @Builder
    public ElectricBike(long bikeNo, String name, String type,
                        double weight, String licensePlate, LocalDate manufacturingDate,
                        String producer, int cost, BikeStatus status, String bikeCode,
                        int batteryPercentage, int loadCycles, int usageTimeRemainingInSecond) {
        super(bikeNo, name, type, weight, licensePlate, manufacturingDate, producer, cost, status, bikeCode);
        if (batteryPercentage < 0 || batteryPercentage > 100) {
            throw new InvalidParameterException("Battery percentage must smaller than 100 and greater than 0");
        }
        this.batteryPercentage = batteryPercentage;
        this.loadCycles = loadCycles;
        this.usageTimeRemainingInSecond = usageTimeRemainingInSecond;
    }

    public static class ElectricBikeBuilder extends BikeBuilder {
        ElectricBikeBuilder() {
            super();
        }
    }

    public void setBatteryPercentage(int batteryPercentage) {
        if (batteryPercentage < 0 || batteryPercentage > 100) {
            throw new InvalidParameterException("Battery percentage must smaller than 100 and greater than 0");
        }
        this.batteryPercentage = batteryPercentage;
    }
}
