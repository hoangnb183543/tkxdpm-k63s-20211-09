package model.dbconnection;

import lombok.extern.java.Log;

import java.sql.Connection;
import java.sql.DriverManager;

@Log
public class SQLLiteConnection {

    private static Connection connection;

    public static Connection getConnection() {
        if (connection != null) {
            return connection;
        }
        try {
            Class.forName("org.sqlite.JDBC");
            String url = "jdbc:sqlite:identifier.sqlite";
            connection = DriverManager.getConnection(url);
            log.info("Connect database successfully");
        } catch (Exception e) {
            log.info(e.getMessage());
        }
        return connection;
    }

    public static void main(String[] args) {
        SQLLiteConnection.getConnection();
    }
}
