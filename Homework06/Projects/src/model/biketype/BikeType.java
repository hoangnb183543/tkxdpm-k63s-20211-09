package model.biketype;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Lớp model loại xe
 * 
 * @author PTHIEU 20.12.2021
 */
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class BikeType {
	private long bikeTypeNo;
	private String bikeTypeName;
	private String bikeTypeCode;
	private String bikeTypeDescription;
	private double depositAmount;
	private String formula;
}
