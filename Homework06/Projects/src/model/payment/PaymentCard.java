package model.payment;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PaymentCard {

    protected String owner;
    protected String cardCode;
    protected String dateExpired;
    protected String cvvCode;

}
