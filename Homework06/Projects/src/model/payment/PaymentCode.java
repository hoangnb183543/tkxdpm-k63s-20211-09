package model.payment;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Getter
public enum PaymentCode {

    SUCCESS("00", "Giao dịch thành công"),
    INVALID_CARD("01", "Thẻ không hợp lệ"),
    NOT_ENOUGH_BALANCE("02", "Thẻ không đủ số dư"),
    INTERNAL_SERVER_ERROR("03", "Internal Server Error"),
    SUSPECT_CHEAT_TRANSACTION("04", "Giao dịch bị nghi ngờ gian lận"),
    NOT_ENOUGH_INFO("05", "Không đủ thông tin giao dịch"),
    MISSING_VERSION_INFO("06", "Thiếu thông tin version"),
    INVALID_AMOUNT("07", "Amount không hợp lệ");

    private String code;
    private String description;

    public static PaymentCode findByCode(String code) {
        for (PaymentCode paymentCode : PaymentCode.values()) {
            if (paymentCode.getCode().equalsIgnoreCase(code)) {
                return paymentCode;
            }
        }
        return null;
    }
}
