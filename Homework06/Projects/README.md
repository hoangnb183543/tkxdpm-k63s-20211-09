#  A rental bike application
## Folder Structure

The workspace contains the following folders, where:

- `src`: the folder to maintain sources
- `assets`: the folder to maintain static resources
- `test`: the folder for testing purpose
- `identifier.sqlite`: the file contain data for app

## Dependency Management
All dependency in project can be installed by maven
### SQLite
Using `sqlite-jdbc-3.7.2.jar`

### Junit
Using `junit-4.13.2.jar`

### Java
Using `Java-FX-15` with `java 11`

### Gson
Using `gson-2.8.5.jar` for parsing json string

### Lombok
Using `lombok-1.8` to avoid boilerplate code

## Add VM arguments
Click on `Run` -> `Run Configurations...`  -> `Java Application`, create a new launch configuration for your project and add these VM arguments:
- For Linux distro:
> `--module-path lib/linux/javafx-sdk-15 --add-modules javafx.controls,javafx.fxml`
- For Windows:
> `--module-path lib/win/javafx-sdk-15 --add-modules javafx.controls,javafx.fxml`
 
# Author
- hungnd - SOICT - HUST
- hieupt - SOICT - HUST
- longnd - SOICT - HUST
- quocpha - SOICT - HUST
- hoangnb - SOICT - HUST