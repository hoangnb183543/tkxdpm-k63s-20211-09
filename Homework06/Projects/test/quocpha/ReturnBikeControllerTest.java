package quocpha;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import controller.ReturnBikeController;
import model.dockingstation.DockingStation;

public class ReturnBikeControllerTest {
	private ReturnBikeController controller;
	
    @Before
    public void setUp() {
        controller = new ReturnBikeController();
    }
    
    @Test
    public void isEnoughCapacityStation() {
    	List<DockingStation> stations = controller.findListDockingStation();

    	Assert.assertTrue(stations.stream().allMatch(station -> station.getCapacity() > 0));
    }
}
