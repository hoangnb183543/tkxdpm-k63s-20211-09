package longnd;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import controller.ParkingController;

public class ValidateAddressTest {

    private ParkingController parkingController;

    @BeforeEach
    void setUp() throws Exception {
    	parkingController = new ParkingController();
    }

    @ParameterizedTest
    @CsvSource({
            "Bai Cat Song Hong,true",
            "22 Le Thanh Nghi,true",
            ",false"
    })
    void test(String name, boolean expected) {
        boolean isValid = parkingController.validateAddress(name);
        Assertions.assertEquals(isValid, expected);
    }
}
