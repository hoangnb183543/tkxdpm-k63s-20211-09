package hoangnb;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import model.dockingstation.DockingStation;
import repository.DockingStationRepository;
import repository.impl.DockingStationRepositoryImpl;

public class DockingStationRepositoryTest {

	DockingStationRepository repository;
	
	public static final List<DockingStation> ALL_DOCKING_STATION = List.of(
			new DockingStation(1L, "Hoang Hai", "So 1, Nguyen Huy Tuong", 100L),
			new DockingStation(2L, "Song Hong", "So 19, Le Van Luong, Ha Noi", 190L),
			new DockingStation(3L, "GAMBIG", "61 Nguyễn Lữ, Phường Khuê Mỹ, Quận Ngũ Hành Sơn, Thành phố Đà Nẵng", 1820L),
			new DockingStation(4L, "NGUYÊN HỒNG", "241 Võ An Ninh, Phường Hoà Xuân, Quận Cẩm Lệ, Thành phố Đà Nẵng", 605L)
			);
	
	@Before
	public void setUp() {
		repository = new DockingStationRepositoryImpl();
	}

	@Test
	public void testFindAllDockingStations() {
		List<DockingStation> listDockingStaion = repository.findAllDockingStations();
		Assert.assertEquals(ALL_DOCKING_STATION.toString(), listDockingStaion.toString());
	}
}
