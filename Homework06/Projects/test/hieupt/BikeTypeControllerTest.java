package hieupt;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import controller.BikeTypeController;
import model.biketype.BikeType;
import utils.rentalprice.RentalPriceConstant;

class BikeTypeControllerTest {
	
	BikeTypeController bikeTypeController;

	@BeforeEach
	void setUp() throws Exception {
		bikeTypeController = new BikeTypeController();
	}

	@Test
	void testGetAllBikeTypes() {
		List<BikeType> expectedList = new ArrayList<BikeType>();
		BikeType bikeType = BikeType.builder()
					        .bikeTypeNo(1)
					        .bikeTypeCode("BT001")
					        .bikeTypeName("Commuter")
					        .bikeTypeDescription("1 saddle, 1 pedal and 1 rear seat")
					        .depositAmount(400000)
					        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":10000,\"every15MinutesNextPrice\":3000}")
					        .build();
		expectedList.add(bikeType);
		
		bikeType = BikeType.builder()
		        .bikeTypeNo(2)
		        .bikeTypeCode("BT002")
		        .bikeTypeName("Tandem")
		        .bikeTypeDescription("2 saddle, 2 pedal and 1 rear seat")
		        .depositAmount(550000)
		        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":15000,\"every15MinutesNextPrice\":4500}")
		        .build();
		expectedList.add(bikeType);
		
		bikeType = BikeType.builder()
		        .bikeTypeNo(3)
		        .bikeTypeCode("BT003")
		        .bikeTypeName("Electric")
		        .bikeTypeDescription("1 saddle, 1 pedal and 1 rear seat. With electric motor")
		        .depositAmount(700000)
		        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":15000,\"every15MinutesNextPrice\":4500}")
		        .build();
		expectedList.add(bikeType);
		
		bikeType = BikeType.builder()
		        .bikeTypeNo(4)
		        .bikeTypeCode("BT004")
		        .bikeTypeName("Twin Electric Bike")
		        .bikeTypeDescription("2 saddle, 2 pedal and 1 rear seat. With electric motor.")
		        .depositAmount(800000)
		        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":20000.0,\"every15MinutesNextPrice\":6000.0}")
		        .build();
		expectedList.add(bikeType);
		
		
		List<BikeType> list = bikeTypeController.getAllBikeTypes();
		
		assertEquals(true, expectedList.retainAll(list));
		
	}

	@Test
	void testGetBikeTypeByNo() {
		BikeType expectedBikeType = BikeType.builder()
		        .bikeTypeNo(1)
		        .bikeTypeCode("BT001")
		        .bikeTypeName("Commuter")
		        .bikeTypeDescription("1 saddle, 1 pedal and 1 rear seat")
		        .depositAmount(400000)
		        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":10000,\"every15MinutesNextPrice\":3000}")
		        .build();
		
		BikeType myBikeType = bikeTypeController.getBikeTypeByNo(1);
		assertEquals(expectedBikeType.toString(), myBikeType.toString());
	}

	@Test
	void testAddBikeType() {
		BikeType bikeType = BikeType.builder()
		        .bikeTypeCode("BT005")
		        .bikeTypeName("abc")
		        .bikeTypeDescription("xyz")
		        .depositAmount(700000)
		        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":10000,\"every15MinutesNextPrice\":3000}")
		        .build();
		
		int count = bikeTypeController.addBikeType(bikeType);
		assertEquals(1, count);
		
		count = bikeTypeController.addBikeType(bikeType);
		assertEquals(0, count);
	}

	@Test
	void testUpdateBikeType() {
		BikeType bikeType = BikeType.builder()
				.bikeTypeNo(5)
		        .bikeTypeCode("BT005")
		        .bikeTypeName("abc")
		        .bikeTypeDescription("xyz ghijk")
		        .depositAmount(700000)
		        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":20000,\"every15MinutesNextPrice\":30000}")
		        .build();
		
		int count = bikeTypeController.updateBikeType(bikeType);
		assertEquals(1, count);
		
		bikeType = BikeType.builder()
				.bikeTypeNo(10)
		        .bikeTypeCode("BT005")
		        .bikeTypeName("abc")
		        .bikeTypeDescription("xyz ghijk")
		        .depositAmount(700000)
		        .formula("{\"freeMinutesDuration\":10,\"first30MinutesPrice\":20000,\"every15MinutesNextPrice\":30000}")
		        .build();
		
		count = bikeTypeController.updateBikeType(bikeType);
		assertEquals(0, count);
	}

	@Test
	void testDeleteBikeType() {
		int count = bikeTypeController.deleteBikeType(5);
		assertEquals(1, count);
		
		count = bikeTypeController.deleteBikeType(5);
		assertEquals(0, count);
	}

	@Test
	void testValidateInfo() {
		Map<String, Object> bikeTypeInfo = new HashMap<String, Object>();
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_CODE, "BT001");
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_NAME, "abc");
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DESCRIPTION, "");
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, "400000");
		bikeTypeInfo.put(RentalPriceConstant.FREE_MINUTES_DURATION, "10");
		bikeTypeInfo.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, "10000");
		bikeTypeInfo.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, "3000");
		
		assertTrue(bikeTypeController.validateInfo(bikeTypeInfo));
		
		// bike type code
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_CODE, null);
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_CODE, "");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_CODE, "    ");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_CODE, "BT001");
		
		
		// bike type name
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_NAME, null);
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_NAME, "");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_NAME, "    ");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_NAME, "abc");
		
		// bike type deposit amount
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, null);
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, "");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, "    ");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, "abc");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, "12a");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(BikeTypeController.BIKE_TYPE_DEPOSIT_AMOUNT, "400000");
		
		// bike type free minutes duration
		bikeTypeInfo.put(RentalPriceConstant.FREE_MINUTES_DURATION, null);
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FREE_MINUTES_DURATION, "");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FREE_MINUTES_DURATION, "    ");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FREE_MINUTES_DURATION, "abc");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FREE_MINUTES_DURATION, "12a");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FREE_MINUTES_DURATION, "10");
		
		// bike type first 30 minutes price
		bikeTypeInfo.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, null);
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, "");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, "    ");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, "abc");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, "12a");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.FIRST_30_MINUTES_PRICE, "10000");
		
		// bike type first 30 minutes price
		bikeTypeInfo.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, null);
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, "");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, "    ");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, "abc");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, "12a");
		assertFalse(bikeTypeController.validateInfo(bikeTypeInfo));
		
		bikeTypeInfo.put(RentalPriceConstant.EVERY_15_MINUTES_NEXT_PRICE, "3000");
	}

}
