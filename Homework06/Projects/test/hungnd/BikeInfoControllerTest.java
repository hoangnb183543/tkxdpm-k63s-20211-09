package hungnd;

import controller.BikeInfoController;
import model.bike.Bike;
import model.bike.BikeStatus;
import model.bike.ElectricBike;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import repository.BikeRepository;
import repository.impl.BikeRepositoryImpl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BikeInfoControllerTest {

    public static final LocalDate MANUFACTURING_DATE = LocalDate.of(2021, 12, 25);
    public static final String GIANT_133_S_PLUS = "Giant 133s Plus";
    public static final String ECOBIKE_0001 = "ECOBIKE0001";
    public static final String ELECTRIC = "Electric";
    public static final double WEIGHT = 90.0;
    public static final String LICENSE_PLATE = "0001";
    public static final String PRODUCER = "Giant";
    public static final int COST = 15000000;
    public static final int BATTERY_PERCENTAGE = 100;
    public static final int LOAD_CYCLES = 3;
    public static final int REMAINING = 36000;
    public static final String COMMUTER = "Commuter";
    public static final String TANDEM = "Tandem";
    public static final int COMMUTER_DEPOSIT_AMT = 400000;
    public static final int ELECTRIC_DEPOSIT_AMT = 550000;
    public static final int TWIN_DEPOSIT_AMT = 700000;
    BikeInfoController controller;
    BikeRepository repository;

    @Before
    public void setUp() {
        controller = new BikeInfoController();
        repository = new BikeRepositoryImpl();
    }

    @Test
    public void updateHiredStatus() {
        List<Bike> bikes = getListBike();
        for (Bike bike : bikes) {
            long bikeNo = repository.save(bike).getBikeNo();
            controller.updateBikeStatusRented(bikeNo);
            Assert.assertEquals(repository.findBikeByNo(bikeNo).getStatus(), BikeStatus.RENTED);
        }
    }

    @Test
    public void updateAvailableStatus() {
        List<Bike> bikes = getListBike();
        for (Bike bike : bikes) {
            long bikeNo = repository.save(bike).getBikeNo();
            controller.updateBikeStatusAvailable(bikeNo);
            Assert.assertEquals(repository.findBikeByNo(bikeNo).getStatus(), BikeStatus.AVAILABLE);
        }
    }

    @Test
    public void updateHiringInProgressStatus() {
        List<Bike> bikes = getListBike();
        for (Bike bike : bikes) {
            long bikeNo = repository.save(bike).getBikeNo();
            controller.updateBikeStatusInProgress(bikeNo);
            Assert.assertEquals(repository.findBikeByNo(bikeNo).getStatus(), BikeStatus.RENTING_IN_PROGRESS);
        }
    }

    private List<Bike> getListBike() {
        Bike availableBike = Bike.builder()
                .name("Available bike")
                .type(ELECTRIC)
                .status(BikeStatus.AVAILABLE)
                .build();
        Bike progressBike = Bike.builder()
                .type(ELECTRIC)
                .name("In progress bike")
                .status(BikeStatus.RENTING_IN_PROGRESS)
                .build();
        Bike rentedBike = Bike.builder()
                .type(ELECTRIC)
                .name("Rented bike")
                .status(BikeStatus.RENTED)
                .build();
        List<Bike> bikes = new ArrayList<>();
        bikes.add(availableBike);
        bikes.add(progressBike);
        bikes.add(rentedBike);
        return bikes;
    }


    @Test
    public void getBikeInfo() {
        ElectricBike eBike = ElectricBike.builder()
                .name(GIANT_133_S_PLUS)
                .bikeCode(ECOBIKE_0001)
                .type(ELECTRIC)
                .weight(WEIGHT)
                .licensePlate(LICENSE_PLATE)
                .manufacturingDate(MANUFACTURING_DATE)
                .producer(PRODUCER)
                .cost(COST)
                .batteryPercentage(BATTERY_PERCENTAGE)
                .loadCycles(LOAD_CYCLES)
                .usageTimeRemainingInSecond(REMAINING)
                .status(BikeStatus.AVAILABLE)
                .build();
        Bike savedBike = repository.save(eBike);

        Map<String, Object> expected = new HashMap<>();
        expected.put(BikeInfoController.BIKE_NO, savedBike.getBikeNo());
        expected.put(BikeInfoController.BIKE_NAME, GIANT_133_S_PLUS);
        expected.put(BikeInfoController.BIKE_CODE, ECOBIKE_0001);
        expected.put(BikeInfoController.BIKE_TYPE, ELECTRIC);
        expected.put(BikeInfoController.WEIGHT, WEIGHT);
        expected.put(BikeInfoController.LICENSE_PLATE, LICENSE_PLATE);
        expected.put(BikeInfoController.MANU_DATE, MANUFACTURING_DATE);
        expected.put(BikeInfoController.PRODUCER, PRODUCER);
        expected.put(BikeInfoController.COST, COST);
        expected.put(BikeInfoController.BATTERY_PERCENTAGE, BATTERY_PERCENTAGE);
        expected.put(BikeInfoController.LOAD_CYCLES, LOAD_CYCLES);
        expected.put(BikeInfoController.USAGE_TIME_REMAINING_IN_SECOND, REMAINING);
        expected.put(BikeInfoController.STATUS, BikeStatus.AVAILABLE.getName());

        Map<String, Object> actual = controller.getBikeInfo(savedBike.getBikeNo());
        Assert.assertEquals(expected, actual);

        long NO_EXIST_BIKE_NO = 10000;
        Assert.assertEquals(new HashMap<>(), controller.getBikeInfo(NO_EXIST_BIKE_NO));
    }

    @Test
    public void isAvailableTest() {
        Bike availableBike = Bike.builder()
                .name("Available bike")
                .type(ELECTRIC)
                .status(BikeStatus.AVAILABLE)
                .build();
        Bike progressBike = Bike.builder()
                .type(ELECTRIC)
                .name("In progress bike")
                .status(BikeStatus.RENTING_IN_PROGRESS)
                .build();
        Bike rentedBike = Bike.builder()
                .type(ELECTRIC)
                .name("Rented bike")
                .status(BikeStatus.RENTED)
                .build();
        Assert.assertTrue(controller.isAvailable(repository.save(availableBike).getBikeNo()));
        Assert.assertFalse(controller.isAvailable(repository.save(progressBike).getBikeNo()));
        Assert.assertFalse(controller.isAvailable(repository.save(rentedBike).getBikeNo()));
    }

    @Test
    public void getDepositAmtTest() {
        Bike bike = Bike.builder()
                .name("Commuter bike")
                .type(COMMUTER)
                .status(BikeStatus.AVAILABLE)
                .build();
        Bike eBike = Bike.builder()
                .name("Electric bike")
                .type(ELECTRIC)
                .status(BikeStatus.AVAILABLE)
                .build();
        Bike tBike = Bike.builder()
                .name("Twin bike")
                .type(TANDEM)
                .status(BikeStatus.AVAILABLE)
                .build();
        Assert.assertEquals(COMMUTER_DEPOSIT_AMT, controller.getDepositAmtByBikeNo(repository.save(bike).getBikeNo()), 0);
        Assert.assertEquals(ELECTRIC_DEPOSIT_AMT, controller.getDepositAmtByBikeNo(repository.save(tBike).getBikeNo()), 0);
        Assert.assertEquals(TWIN_DEPOSIT_AMT, controller.getDepositAmtByBikeNo(repository.save(eBike).getBikeNo()), 0);
    }
}
