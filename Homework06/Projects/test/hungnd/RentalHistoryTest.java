package hungnd;

import controller.RentalHistoryController;
import exception.EcoBikeException;
import model.bike.Bike;
import model.bike.BikeStatus;
import model.rentalhistory.BikeRentalHistory;
import model.rentalhistory.UserAction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import repository.BikeRepository;
import repository.RentalHistoryRepository;
import repository.impl.BikeRepositoryImpl;
import repository.impl.RentalHistoryRepositoryImpl;

import java.time.LocalDateTime;
import java.util.List;

public class RentalHistoryTest {

    public static final String COMMUTER = "Commuter";
    RentalHistoryRepository repository;

    RentalHistoryController controller;

    BikeRepository bikeRepository;

    @Before
    public void setUp() {
        repository = new RentalHistoryRepositoryImpl();
        controller = new RentalHistoryController();
        bikeRepository = new BikeRepositoryImpl();
    }

    @Test
    public void findByUserNo() {
        long userNo = 1;
        List<BikeRentalHistory> histories = repository.findHistoriesByUserNo(userNo);
        System.out.println(histories);
        histories.forEach(history -> Assert.assertEquals(userNo, history.getUserNo()));
    }

    @Test
    public void insertNew() {
        Bike availableBike = Bike.builder()
                .name("TEST")
                .type(COMMUTER)
                .status(BikeStatus.AVAILABLE)
                .build();
        Bike tempAvailableBike = bikeRepository.save(availableBike);
        Assert.assertNotNull(controller.saveRentHistory(tempAvailableBike.getBikeNo()));
    }

    @Test
    public void isHiring() {
        Bike availableBike = Bike.builder()
                .name("TEST")
                .type(COMMUTER)
                .status(BikeStatus.AVAILABLE)
                .build();
        Bike tempAvailableBike = bikeRepository.save(availableBike);
        Assert.assertNotNull(controller.saveRentHistory(tempAvailableBike.getBikeNo()));
        Assert.assertTrue(controller.isRenting());

        Bike rentedBike = Bike.builder()
                .name("TEST")
                .type(COMMUTER)
                .status(BikeStatus.RENTED)
                .build();
        Bike tempRentedBike = bikeRepository.save(rentedBike);
        Assert.assertNotNull(controller.saveReturnHistory(tempRentedBike.getBikeNo()));
        Assert.assertFalse(controller.isRenting());
    }
}
