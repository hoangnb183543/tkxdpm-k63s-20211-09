package hungnd;

import model.payment.PaymentCode;
import model.payment.PaymentTransaction;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import repository.PaymentTransactionRepository;
import repository.impl.PaymentTransactionRepositoryImpl;

import java.time.LocalDateTime;
import java.util.Random;

public class PaymentTransactionRepositoryTest {

    PaymentTransactionRepository repository;

    @Before
    public void setUp() {
        repository = new PaymentTransactionRepositoryImpl();
    }

    @Test
    public void insertNew() {
        for (PaymentCode code : PaymentCode.values()) {
            int leftLimit = 97; // letter 'a'
            int rightLimit = 122; // letter 'z'
            int targetStringLength = 10;
            Random random = new Random();

            String randomTransactionNo = random.ints(leftLimit, rightLimit + 1)
                    .limit(targetStringLength)
                    .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                    .toString();
            PaymentTransaction transaction = PaymentTransaction.builder()
                    .createdAt(LocalDateTime.now())
                    .paymentCode(code)
                    .content("TEST")
                    .amount(10.5)
                    .transactionNo(randomTransactionNo)
                    .build();
            Assert.assertNotNull(repository.save(transaction));
        }
    }
}
